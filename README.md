# MAP401 - Extraction de contours

Projet de groupe avec:

- Quentin FAIDIDE
- Alexis NANTERNE


Ce projet intègre des mécaniques de **Continuous Integration** (voir section dédiée plus bas). 

Référez vous à la section "**Professeurs**" pour le détails du téléchargement des différentes parties.

## Contenu
- Paquetage **geom2d**: implémentations de fonction de géométrie 2D
- Paquetage **test_geom2d**: tests du paquetage geom2d
- Paquetage **image**: implémentations de fonction de gestion des images
- Paquetage **test_image**: tests du paquetage image
- Paquetage **calcul_contour**: implémentations de fonction de calcul des contours
- Paquetage **test_calcul_contour**: tests du paquetage calcul_contour

## Technologies

Nous utilisons:

* [**clang**] - compilateur
* [**clang-format**] - nous permet de linter/formatter le code (pas contraignant dans la CONTINUOUS INTEGRATION!!)
* [**git**] - gestionnaire de version
* [**gitlab**] - serveur git avec CONTINUOUS INTEGRATION / CONTINOUS DELIVERY
* [**docker**] - pour nos pipeline de CI
* [**gdb**] - pour le debug
* [**valgrind**] - pour le debug
* [**visual-studio-code-oss**] - version open source de visual studio code
* [**nedit**] - pâle copie de gedit

## Installation

Make, clang, et la librairie standard C sont nécessaires. Gcc peut être utilisé avec des flags supplémentaire mais c'est à votre charge de configurer un alias de clang à gcc avec les flags ou d'éditer le makefile.

```sh
$ cd build
$ make
```

Pour réaliser les tests...

```sh
$ cd build
$ make
$ cd ../tests
$ ./tests.sh
```

### Utilisation

Les éxécutables sont dans le dossier build/bin.

## Professeurs

La tâche n est appellée "partie_n", pour la télécharger, il faut lancer la commande:
```sh
$ git clone -b 'partie_n' --single-branch --depth 1 https://gitlab.com/Faidide/map401-extraction-contours.git
```
Pensez à remplacer le n dans "partie_n" par le numéro de la tâche que vous voulez télécharger.

## Étudiants

La tâche n est appellée "partie_n", pour assigner un tag à votre version, il faut récupérer le numéro de commit désiré dans les logs:

```sh
$ git log
```

Et y copier le début du numéro du commit que vous voulez taguer pour y placer le tag avant de pousser les changements au serveur:

```sh
$ git tag partie_n f4ba1fc
$ git push origin --tags
```

Pensez à remplacer le n dans "partie_n" par le numéro de la tâche que vous voulez rendre.

## Continuous Integration

- CONTINUOUS INTEGRATION gitlab (.gitlab-ci.yml):
  - Pipeline de Build: fais une build dans un contenaire docker, échoue si erreur de compilation
  - Pipeline de Test: regarde la sortie de ./tests.sh dans un contenaire docker et vérifie les codes de retours
- Si un changement est push sur git, on passe automatiquement dans les pipelines pour effectuer les tests. 
- Si un pipeline fail, on envoie un mail aux membres du projet avec les logs. 


**Free Software, Hell Yeah!**
