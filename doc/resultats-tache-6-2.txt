image_poly_tache6:
Calcul du multi contours avec sauvegarde en memoire.
Nombre de contours: 3
Nombre de points: 491
Nombre de segments: 488
Simplification du contour (d=1.0) et affichage du résumé...
Nombre de contours: 3
Nombre de points: 71
Nombre de segments: 68
Simplification du contour (d=2.0) et affichage du résumé...
Nombre de contours: 3
Nombre de points: 47
Nombre de segments: 44

elephant-gotlib:
Calcul du multi contours avec sauvegarde en memoire.
Nombre de contours: 313
Nombre de points: 115877
Nombre de segments: 115564
Simplification du contour (d=1.0) et affichage du résumé...
Nombre de contours: 313
Nombre de points: 13502
Nombre de segments: 13189
Simplification du contour (d=2.0) et affichage du résumé...
Nombre de contours: 313
Nombre de points: 4594
Nombre de segments: 4281

goudyini-A:
Calcul du multi contours avec sauvegarde en memoire.
Nombre de contours: 258
Nombre de points: 73702
Nombre de segments: 73444
Simplification du contour (d=1.0) et affichage du résumé...
Nombre de contours: 258
Nombre de points: 5593
Nombre de segments: 5335
Simplification du contour (d=2.0) et affichage du résumé...
Nombre de contours: 258
Nombre de points: 2971
Nombre de segments: 2713

JoaquimHock-LesArbres:
Calcul du multi contours avec sauvegarde en memoire.
Nombre de contours: 543
Nombre de points: 231827
Nombre de segments: 231284
Simplification du contour (d=1.0) et affichage du résumé...
Nombre de contours: 543
Nombre de points: 27115
Nombre de segments: 26572
Simplification du contour (d=2.0) et affichage du résumé...
Nombre de contours: 543
Nombre de points: 13633
Nombre de segments: 13090

cheval:
Calcul du multi contours avec sauvegarde en memoire.
Nombre de contours: 59
Nombre de points: 32061
Nombre de segments: 32002
Simplification du contour (d=1.0) et affichage du résumé...
Nombre de contours: 59
Nombre de points: 3216
Nombre de segments: 3157
Simplification du contour (d=2.0) et affichage du résumé...
Nombre de contours: 59
Nombre de points: 1440
Nombre de segments: 1381

papillon2:
Calcul du multi contours avec sauvegarde en memoire.
Nombre de contours: 140
Nombre de points: 18352
Nombre de segments: 18212
Simplification du contour (d=1.0) et affichage du résumé...
Nombre de contours: 140
Nombre de points: 2053
Nombre de segments: 1913
Simplification du contour (d=2.0) et affichage du résumé...
Nombre de contours: 140
Nombre de points: 1177
Nombre de segments: 1037
