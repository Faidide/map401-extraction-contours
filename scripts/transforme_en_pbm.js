fs = require('fs');

if(process.argv.length!=4) {
    console.log("Usage: "+process.argv[0]+ " "+ process.argv[1] + " [ascii_input] [output_file.pbm]");
    process.exit(0);
} 

let converted_image = String(fs.readFileSync(process.argv[2])).replace(/ /g,"0").replace(/[^0\n]/g,"1");

let lines = converted_image.split("\n");
let width = converted_image.indexOf("\n")+20;

for(let i=0;i<lines.length;i++) {
    lines[i] = "0000000000"+lines[i]+"0000000000";
}

let ligne_vide = "";
for(let i=0;i<width;i++) {
    ligne_vide = ligne_vide+"0";
}

for(let i=0;i<5;i++) {
    lines.unshift(ligne_vide);
    lines.push(ligne_vide);
}

let rendering = "";
for(let i=0;i<lines.length;i++) {
    rendering = rendering + "\n" + lines[i];
}

rendering = "P1\n" + width + " "+ lines.length + rendering; 

console.log(rendering);

fs.writeFileSync(process.argv[3], rendering);