#include "bezier.h"
#include "geom2d.h"
#include <float.h>
#include <math.h>

/* Prends un contours et retourne un coutours de Bezier 2D*/
ContoursBezier2D *simplification_en_bezier_2D (Contours *mon_contour, int j1,
                                               int j2, double distance) {
  // static pour éviter de surcharger la frame avec des variables locales à
  // chaque appel de la fonction
  static double dmax = 0.0f;
  static double dj = 0.0f;
  static int j;
  int k = j1;
  int n = j2 - j1;
  int i;
  double ti;

  ContoursBezier2D *nouveau_contour =
      approximation_bezier_2d (mon_contour, j1, j2);

  dmax = 0.0f;

  for (j = j1 + 1; j <= j2; j++) {
    i = j - j1;
    ti = (double)i / (double)n;

    // distance point-bezier
    dj = distance_point_bezier (mon_contour->tab[j], &nouveau_contour->tab[0],
                                ti);

    // si la distance est la plus grand jamais vue, la sauvegarder
    if (dmax < dj) {
      dmax = dj;
      k = j;
    }
  }

  // si notre plus grande distance ne dépasse pas d
  if (dmax <= distance) {
    // on retourne la courbe générée au début
    return nouveau_contour;
    // sinon, dmax est plus grand que d
  } else {
    // on libère la mémoire du contours calculé au début
    free (nouveau_contour->tab);
    free (nouveau_contour);
    // on va diviser notre tâche en deux
    // calcul du contour simplifié gauche
    ContoursBezier2D *L1 =
        simplification_en_bezier_2D (mon_contour, j1, k, distance);
    // calcul du contour simplifié droit
    ContoursBezier2D *L2 =
        simplification_en_bezier_2D (mon_contour, k, j2, distance);

    // on concatene L2 dans L1
    concatener_contours_bezier_2d (L1, L2);

    // on retourne la concaténation des deux
    return L1;
  }
}

/* Prends un contours et retourne un coutours de Bezier 3D*/
ContoursBezier3D *simplification_en_bezier_3D (Contours *mon_contour) {}

/* Prends un ensemble de contours et les transforme en plusieurs contours
 * beziers 2D*/
MultiContoursBezier2D *
simplification_bezier_multi_2D (MultiContours *mes_contours, double distance) {

  // on alloue une structure pour stocker le pointeur vers le tableau et ses
  // tailles
  MultiContoursBezier2D *contours_image =
      malloc (sizeof (int) * 2 + sizeof (ContoursBezier2D *));
  // si l'allocation a échouée
  if (contours_image == NULL || contours_image == 0x0) {
    // afficher l'erreur
    perror ("malloc: ");
    // on appelle notre macro qui gère les erreurs
    ERREUR_FATALE (
        "Erreur lors de l'allocation de la structure MultiContoursBezier2d.");
    return NULL;
  }

  // et on alloue le tableau de contours (pointeurs vers des structures
  // Contours)
  contours_image->contours =
      malloc (mes_contours->max_length * sizeof (ContoursBezier2D));
  // si l'allocation a échouée
  if (contours_image->contours == NULL || contours_image->contours == 0x0) {
    // afficher l'erreur
    perror ("malloc: ");
    // on appelle notre macro qui gère les erreurs
    ERREUR_FATALE ("Erreur lors de l'allocation du tableau de la structure "
                   "MultiContoursBezier2d.");
    return NULL;
  }

  // on initialise la longueur par défaut et la longueur max
  contours_image->length = mes_contours->length;
  contours_image->max_length = mes_contours->max_length;

  /////////////////////////////////
  // SIMPLIFICATION DES CONTOURS //
  /////////////////////////////////
  int i, j;
  // pour chaque contour
  for (i = 0; i < mes_contours->length; i++) {
    // en générer un nouveau avec notre fonction de simplification
    ContoursBezier2D *nouveau_contours = simplification_en_bezier_2D (
        &mes_contours->contours[i], 0, mes_contours->contours[i].length - 1,
        distance);

    // si notre tableau est à sa taille maximum
    if (contours_image->length >= contours_image->max_length) {
      // calcul de la nouvelle taille
      int nouv_taille = (contours_image->max_length + NB_BEZIER_BLOC_SIZE) *
                        (sizeof (ContoursBezier2D));
      // on test l'allocation d'une nouvelle zone memoire
      ContoursBezier2D *nouvelle_alloc =
          realloc (contours_image->contours, nouv_taille);
      // on vérifie si l'allocation n'a pas échoué
      if (nouvelle_alloc == NULL || nouvelle_alloc == 0x0) {
        perror ("realloc: ");
        ERREUR_FATALE ("Erreur lors de la réallocation d'un tableau pour "
                       "MultiContoursBezier2d.");
        return NULL;
      } else {
        // réassigne la nouvelle zone réservée
        contours_image->contours = nouvelle_alloc;
        // incrémentation de la taille max
        contours_image->max_length =
            contours_image->max_length + NB_CONTOURS_BLOC_SIZE;
      }
    }

    // et on ajoute le contours crée à la structure MultiContours

    // on initialise le contours i
    // il a une taille encore nulle
    contours_image->contours[i].length = 0;
    // sa taille max par défaut est celle d'un de nos "blocks" de réallocation
    contours_image->contours[i].max_length = BEZIER_BLOC_SIZE;
    // on alloue ensuite l'espace nécessaire pour le tableau de points
    contours_image->contours[i].tab =
        malloc (BEZIER_BLOC_SIZE * sizeof (Bezier2D));
    // si l'allocation a échouée
    if (contours_image->contours[i].tab == NULL ||
        contours_image->contours[i].tab == 0x0) {
      // afficher l'erreur
      perror ("malloc: ");
      // on appelle notre macro qui gère les erreurs
      ERREUR_FATALE ("Erreur lors de l'allocation du tableau de la structure "
                     "Contours dans l'objet MultiContoursBezier2d.");
      return NULL;
    }

    // pour chaque point du contours simplifié
    for (j = 0; j < nouveau_contours->length; j++) {
      // le memoriser dans la nouvelle structure MultiContours
      memoriser_bezier_2d (&contours_image->contours[i],
                           nouveau_contours->tab[j]);
    }

    free (nouveau_contours->tab);
    free (nouveau_contours);
  }

  return contours_image;
}

/* Prends un ensemble de contours et les transforme en plusieurs contours
 * beziers 3D*/
MultiContoursBezier3D *
simplification_bezier_multi_3D (MultiContours *contours_structure,
                                double distance) {}

/* Calcul la position d'un point sur une courbe de bezier */
void calculer_bezier_2D (Bezier2D *courbe, double t,
                         Coordonnees_point *destination) {
  destination->x = pow (1 - t, 2) * courbe->c0.x +
                   2 * t * (1 - t) * courbe->c1.x + pow (t, 2) * courbe->c2.x;
  destination->y = pow (1 - t, 2) * courbe->c0.y +
                   2 * t * (1 - t) * courbe->c1.y + pow (t, 2) * courbe->c2.y;
  return;
}

/* Calcul la position d'un point sur une courbe de bezier */
void calculer_bezier_3D (Bezier3D *courbe, double t,
                         Coordonnees_point *destination) {
  destination->x =
      pow (1 - t, 3) * courbe->c0.x + 3 * t * pow (1 - t, 2) * courbe->c1.x +
      3 * pow (t, 2) * (1 - t) * courbe->c2.x + pow (t, 3) * courbe->c3.x;
  destination->y =
      pow (1 - t, 3) * courbe->c0.y + 3 * t * pow (1 - t, 2) * courbe->c1.y +
      3 * pow (t, 2) * (1 - t) * courbe->c2.y + pow (t, 3) * courbe->c3.y;
  return;
}

Image generer_courbe_bezier_2D (Bezier2D *courbe, double grain, UINT L,
                                UINT H) {
  Image nouvelle_image = creer_image (L, H);
  double t;
  Coordonnees_point point_courant;

  for (t = 0.0f; t <= 1.0; t += grain) {
    calculer_bezier_2D (courbe, t, &point_courant);
    set_pixel_image (nouvelle_image, (int)point_courant.x, (int)point_courant.y,
                     NOIR);
  }

  return nouvelle_image;
}

void redessiner_courbe_bezier_2D (Bezier2D *courbe, double grain, UINT L,
                                  UINT H, Image img) {
  double t;
  Coordonnees_point point_courant;

  for (t = 0.0f; t <= 1.0; t += grain) {
    calculer_bezier_2D (courbe, t, &point_courant);
    set_pixel_image (img, (int)point_courant.x, (int)point_courant.y, NOIR);
  }

  return;
}

Image generer_image_multi_bezier_2d (MultiContoursBezier2D *courbes,
                                     double grain, UINT L, UINT H) {
  Image nouvelle_image = creer_image (L, H);
  double t;
  int i, j;

  for (i = 0; i < courbes->length; i++) {
    // i va beaucoup trop loin parfois
    for (j = 0; j < courbes->contours[i].length; j++) {
      redessiner_courbe_bezier_2D (&courbes->contours[i].tab[j], grain, L, H,
                                   nouvelle_image);
    }
  }

  return nouvelle_image;
}

Image generer_courbe_bezier_3D (Bezier3D *courbe, double grain, UINT L,
                                UINT H) {
  Image nouvelle_image = creer_image (L, H);
  double t;
  Coordonnees_point point_courant;

  for (t = 0.0f; t <= 1.0; t += grain) {
    calculer_bezier_3D (courbe, t, &point_courant);
    set_pixel_image (nouvelle_image, (int)point_courant.x, (int)point_courant.y,
                     NOIR);
  }

  return nouvelle_image;
}

/* Retourne un ensemble de courbes de bézier approximant mon_contour entre j1 et
 * j2 en une seule courbe */
ContoursBezier2D *approximation_bezier_2d (Contours *mon_contour, int j1,
                                           int j2) {
  ContoursBezier2D *nouveau_contour = creer_tableau_beziers_vide ();
  nouveau_contour->length = 1;

  // on calcule la longueur du contour à simplifier
  int n = j2 - j1;
  // si la longueur est nulle
  if (n == 0) {
    // on renvoie une erreur et on arrête
    ERREUR_FATALE ("Erreur, vous essayer de simplifier un point unique.");
    return NULL;
    // si la longueur est de 1 (une droite)
  } else if (n == 1) {
    // on assigne le point C0 au point de rang j1
    nouveau_contour->tab[0].c0.x = mon_contour->tab[j1].x;
    nouveau_contour->tab[0].c0.y = mon_contour->tab[j1].y;
    // on assigne le point C1 entre le point j1 et j2
    nouveau_contour->tab[0].c1.x =
        (mon_contour->tab[j1].x + mon_contour->tab[j2].x) / 2.0;
    nouveau_contour->tab[0].c1.y =
        (mon_contour->tab[j1].y + mon_contour->tab[j2].y) / 2.0;
    // on assigne le point C2 au point de range j2
    nouveau_contour->tab[0].c2.x = mon_contour->tab[j2].x;
    nouveau_contour->tab[0].c2.y = mon_contour->tab[j2].y;
    // on retourne la structure ContoursBezier2D avec notre nouveau Bezier
    // dedans
    return nouveau_contour;
    // sinon, il va falloir calculer le meilleur point C1
  } else {
    // on assigne le point C0 au point de rang j1
    nouveau_contour->tab[0].c0.x = mon_contour->tab[j1].x;
    nouveau_contour->tab[0].c0.y = mon_contour->tab[j1].y;
    // on assigne le point C2 au point de range j2
    nouveau_contour->tab[0].c2.x = mon_contour->tab[j2].x;
    nouveau_contour->tab[0].c2.y = mon_contour->tab[j2].y;

    // calcul du point C1 idéal
    // creation d'un tableau pour tous les résultats possibles
    static double wi;
    static double t;
    static double somme_wi;
    static Coordonnees_point somme_ci;
    somme_ci.x = 0.0;
    somme_ci.y = 0.0;
    somme_wi = 0.0;
    // pour chaque i allant de 1 à n-1
    int i;
    for (i = 1; i < n; i++) {
      // calcul du poids en fonction de la position
      t = (double)i / (double)n;
      wi = 2 * t * (1 - t);
      somme_wi += wi;

      // calculer le point C1 idéal et le ranger dans notre tableau
      somme_ci.x += wi * ((mon_contour->tab[j1 + i].x -
                           pow (1 - t, 2) * mon_contour->tab[j1].x -
                           mon_contour->tab[j2].x * pow (t, 2)) /
                          (2 * t * (1 - t)));
      somme_ci.y += wi * ((mon_contour->tab[j1 + i].y -
                           pow (1 - t, 2) * mon_contour->tab[j1].y -
                           mon_contour->tab[j2].y * pow (t, 2)) /
                          (2 * t * (1 - t)));
    }

    // pour finir calculer la moyenne pondérée en fonction des wi avec la
    // formule vu en cours
    nouveau_contour->tab[0].c1.x = somme_ci.x / somme_wi;
    nouveau_contour->tab[0].c1.y = somme_ci.y / somme_wi;

    // ancien stockage du n pour calculer les ti pour toutes les distances
    // nouveau_contour->tab[0].nb_origine = n;

    // on retourne la structure ContoursBezier2D avec notre nouveau Bezier
    // dedans
    return nouveau_contour;
  }
}

ContoursBezier2D *creer_tableau_beziers_vide () {

  // alloue les deux entiers et le pointeur nécessaires pour la structure
  ContoursBezier2D *contours_image =
      malloc (sizeof (int) * 2 + sizeof (Bezier2D *));

  // si l'allocation a échouée
  if (contours_image == NULL || contours_image == 0x0) {
    // afficher l'erreur
    perror ("malloc: ");
    // on appelle notre macro qui gère les erreurs
    ERREUR_FATALE (
        "Erreur lors de l'allocation de la structure ContoursBezier2D.");
    return NULL;
  }

  // alloue l'espace initial du tableau
  contours_image->tab = malloc (BEZIER_BLOC_SIZE * sizeof (Bezier2D));

  // si l'allocation a échouée
  if (contours_image->tab == NULL || contours_image->tab == 0x0) {
    // afficher l'erreur
    perror ("malloc: ");
    // on appelle notre macro qui gère les erreurs
    ERREUR_FATALE (
        "Erreur lors de l'allocation du tableau de la structure Bezier2D.");
    return NULL;
  }

  // met la longueur à zéro et la longueur max à notre taille initiale
  contours_image->length = 0;
  contours_image->max_length = BEZIER_BLOC_SIZE;

  return contours_image;
}

double distance_point_bezier (Coordonnees_point point, Bezier2D *bezier2d,
                              double t) {
  static Coordonnees_point ct;

  // version avec ti en param
  calculer_bezier_2D (bezier2d, t, &ct);
  return distance ((Point){point.x, point.y}, (Point){ct.x, ct.y});
}

void concatener_contours_bezier_2d (ContoursBezier2D *L1,
                                    ContoursBezier2D *L2) {
  // on teste tout d'abord si les chemins peuvent être mis bout à bouts
  if (L1->tab[L1->length - 1].c2.x != L2->tab[0].c0.x ||
      L1->tab[L1->length - 1].c2.y != L2->tab[0].c0.y) {
    ERREUR_FATALE (
        "Erreur, vous essayer de concatener deux courbes de beziers qui ne "
        "sont pas liées.");
    return;
  }

  // si tout est ok
  int i;
  // pour chaque courbe de L2
  for (i = 0; i < L2->length; i++) {
    // memoriser le point de L2 dans L1
    memoriser_bezier_2d (L1, L2->tab[i]);
  }

  free (L2->tab);
  free (L2);

  return;
}

void memoriser_bezier_2d (ContoursBezier2D *contours_image, Bezier2D courbe) {
  // si la taille est égale à la taille max
  if (contours_image->length >= contours_image->max_length) {
    // on essaye de réallouer une taille de bloc mémoire plus grand
    Bezier2D *nouvelle_alloc = realloc (
        contours_image->tab,
        (contours_image->max_length + BEZIER_BLOC_SIZE) * sizeof (Bezier2D));
    // si l'allocation a échouée
    if (nouvelle_alloc == NULL || nouvelle_alloc == 0x0) {
      // afficher l'erreur
      perror ("realloc: ");
      // on appelle notre macro qui gère les erreurs
      ERREUR_FATALE (
          "Erreur lors de la réallocation d'un tableau (CoutoursBezier2D).");
      return;
    } else {
      // si notre allocation a bien marché
      // on fait pointer notre tableau vers la potentielle nouvelle addresse
      contours_image->tab = nouvelle_alloc;
      // et on met à jour la taille max
      contours_image->max_length =
          contours_image->max_length + BEZIER_BLOC_SIZE;
    }
  }

  // on oublie pas d'incrémenter la taille
  contours_image->length++;
  // une fois qu'on est sur d'avoir de la place
  // on ajoute la nouvelle position au contour
  contours_image->tab[contours_image->length - 1] = courbe;

  // et la fonction se termine ici
  return;
}

/* convertis notre structure de contorus beziers en une image eps */
void ecrire_bezier_eps_2d (MultiContoursBezier2D *mon_contour, char chemin[],
                           UINT l, UINT h) {
  FILE *destination;

  destination = fopen (chemin, "w+");
  if (destination == NULL) {
    perror ("ecrire_contours");
    ERREUR_FATALE ("Erreur");
    return;
  }

  Bezier3D *tampon = malloc(sizeof(Bezier3D));

  fprintf (destination, "%!PS-Adobe-3.0 EPSF-3.0\n");

  fprintf (destination, "%%%%BoundingBox: 0 0 %d %d\n", l, h);

  int j;
  for (j = 0; j < mon_contour->length; j++) {

    // instruction a ecrire pour chaque contour
    fprintf (destination, "%f %f moveto ", mon_contour->contours[j].tab[0].c0.x,
             (h - mon_contour->contours[j].tab[0].c0.y));

    for (int i = 0; i < mon_contour->contours[j].length; i++) {
      // instruction à écrire pour chaque point
      // convertion en bezier 3d
      bezier_2d_to_3d (&mon_contour->contours[j].tab[i], tampon);
      // affichage
      fprintf (destination, "%f %f %f %f %f %f curveto ", tampon->c1.x,
               (h - tampon->c1.y), tampon->c2.x, (h - tampon->c2.y), tampon->c3.x,
               (h - tampon->c3.y));
      // fprintf(destination, "%f %f\n", mon_contour->contours[j].tab[i].x,
      // mon_contour->contours[j].tab[i].y);
    }
  }

  fprintf (destination, "\nfill \n");

  fprintf (destination, "showpage\n");

  fclose (destination);

  return;
}

void bezier_2d_to_3d (Bezier2D *origine, Bezier3D *destination) {
  // nouv_c0 = c0
  destination->c0.x = origine->c0.x;
  destination->c0.y = origine->c0.y;

  // nouv_c3 = c2
  destination->c3.x = origine->c2.x;
  destination->c3.y = origine->c2.y;

  // nouv_c1 = c0 + (2/3)*(c1-c0)
  destination->c1.x = origine->c0.x * (1.0/3.0) + (2.0 / 3.0) * (origine->c1.x);
  destination->c1.y = origine->c0.y * (1.0 / 3.0) + (2.0 / 3.0) * (origine->c1.y);

  // nouv_c2 = c2 + (2/3)*(c1-c2)
  destination->c2.x = (2.0 / 3.0) * (origine->c1.x) + (1.0 / 3.0) * (origine->c2.x);
  destination->c2.y = (2.0 / 3.0) * (origine->c1.y) + (1.0 / 3.0) * (origine->c2.y);

  return;
}

void afficher_infos_bezier_2d (MultiContoursBezier2D *contours_image) {
  int nb_points = 0;
  int nb_segments = 0;
  int i;

  // pour chaque contours dans la structure
  for (i = 0; i < contours_image->length; i++) {
    // additionner le nombre de point
    nb_points += contours_image->contours[i].length + 1;
    // additionner le nombre de segments
    nb_segments += contours_image->contours[i].length;
  }

  // afficher les informations récoltées
  printf ("Nombre de contours: %d\n", contours_image->length);
  printf ("Nombre de points: %d\n", nb_points);
  printf ("Nombre de segments: %d\n", nb_segments);

  // fin de la fonction
  return;
}