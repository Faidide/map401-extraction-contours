/*////////////////////////////////////////////////////////////////////////////

         /$$      /$$  /$$$$$$  /$$$$$$$  /$$   /$$  /$$$$$$    /$$
        | $$$    /$$$ /$$__  $$| $$__  $$| $$  | $$ /$$$_  $$ /$$$$
        | $$$$  /$$$$| $$  \ $$| $$  \ $$| $$  | $$| $$$$\ $$|_  $$
        | $$ $$/$$ $$| $$$$$$$$| $$$$$$$/| $$$$$$$$| $$ $$ $$  | $$
        | $$  $$$| $$| $$__  $$| $$____/ |_____  $$| $$\ $$$$  | $$
        | $$\  $ | $$| $$  | $$| $$            | $$| $$ \ $$$  | $$
        | $$ \/  | $$| $$  | $$| $$            | $$|  $$$$$$/ /$$$$$$
        |__/     |__/|__/  |__/|__/            |__/ \______/ |______/

/////////////////////////////////////////////////////////////////////////// */
// Fichier: bezier.h
// Description: Descriptions des fonctions qui manipules les courbes de beziers
///////////////////////////////////////////////////////////////////////////

#ifndef _SIMPLIFICATION_H_
#define _SIMPLIFICATION_H_

#include "calcul_contour.h"
#include "types_macros.h"

/* Taille des blocs pour les reallocs des tableaux */
#define BEZIER_BLOC_SIZE 3000
#define NB_BEZIER_BLOC_SIZE 20

/* Structure qui représente une courbe de bézier seule de degré 2*/
typedef struct Bezier2D_ {
  Coordonnees_point c0, c1, c2;
} Bezier2D;

/* Structure qui représente une courbe de bézier seule de degré 3*/
typedef struct Bezier3D_ {
  Coordonnees_point c0, c1, c2, c3;
} Bezier3D;

/* Structure qui représente une liste de courbes de bézier de dimension 2 dans
 * un tableau*/
typedef struct ContoursBezier2D_ {
  Bezier2D *tab;
  // taille du tableau
  int length;
  // taille maximale alloué en mémoire pour le moment
  int max_length;
} ContoursBezier2D;

/* Structure qui représente une liste de listes de courbes de bézier de
 * dimension 2 dans un tableau*/
typedef struct MultiContoursBezier2D_ {
  ContoursBezier2D *contours;
  // taille du tableau
  int length;
  // taille maximale alloué en mémoire pour le moment
  int max_length;
} MultiContoursBezier2D;

/* Structure qui représente une liste de courbes de bézier de dimension 3 dans
 * un tableau*/
typedef struct ContoursBezier3D_ {
  Bezier3D *tab;
  // taille du tableau
  int length;
  // taille maximale alloué en mémoire pour le moment
  int max_length;
} ContoursBezier3D;

/* Structure qui représente une liste de listes de courbes de bézier de
 * dimension 3 dans un tableau*/
typedef struct MultiContoursBezier3D_ {
  ContoursBezier3D *contours;
  // taille du tableau
  int length;
  // taille maximale alloué en mémoire pour le moment
  int max_length;
} MultiContoursBezier3D;

/* Prends un contours et retourne une liste de courbes de Bezier la plus proche
 * de tous les points */
ContoursBezier2D *simplification_en_bezier_2D (Contours *mon_contour, int j1,
                                               int j2, double distance);
ContoursBezier3D *simplification_en_bezier_3D (Contours *mon_contour);

/* Prends un ensemble de contours et les transforme en plusieurs listes de
 * courbes beziers*/
MultiContoursBezier2D *
simplification_bezier_multi_2D (MultiContours *contours_structure,
                                double distance);
MultiContoursBezier3D *
simplification_bezier_multi_3D (MultiContours *contours_structure,
                                double distance);

/* Calcul la position d'un point sur une courbe de bezier */
void calculer_bezier_2D (Bezier2D *courbe, double t,
                         Coordonnees_point *destination);
void calculer_bezier_3D (Bezier3D *courbe, double t,
                         Coordonnees_point *destination);

/* Génère une image et dessine la courbe de bézier envoyée */
Image generer_courbe_bezier_2D (Bezier2D *courbe, double grain, UINT L, UINT H);
Image generer_courbe_bezier_3D (Bezier3D *courbe, double grain, UINT L, UINT H);

/* Fais une approximation d'un contours avec une courbe de bézier */
ContoursBezier2D *approximation_bezier_2d (Contours *mon_contour, int j1,
                                           int j2);

/* Crée un tableau de contours beziers vide */
ContoursBezier2D *creer_tableau_beziers_vide ();

/* Calcule la distance en un point et une courbe de bezier (approximativement)
 */
double distance_point_bezier (Coordonnees_point point, Bezier2D *bezier2d,
                              double t);

/* Ajoute une courbe de bézier à une liste de contours de bézier */
/* Indispensable pour alléger le code de la gestion des réallocations de
 * tableaux par blocs*/
void memoriser_bezier_2d (ContoursBezier2D *contours_image, Bezier2D courbe);

/* Met les courbes de béziers de la première liste dans la deuxième */
void concatener_contours_bezier_2d (ContoursBezier2D *L1, ContoursBezier2D *L2);

/* Transforme une courbe de bézier 2 dimensions en 3 */
void bezier_2d_to_3d (Bezier2D *origine, Bezier3D *destination);

/* Crée un fichier EPS à partir d'un ensemble de contours (listes de listes de
 * béziers) avec un remplissage */
void ecrire_bezier_eps_2d (MultiContoursBezier2D *mon_contour, char chemin[],
                           UINT l, UINT h);

/* Retourne une image qui va dessiner toutes les courbes béziers de l'ensemble
 * de listes de béziers qui lui sont donnés */
Image generer_image_multi_bezier_2d (MultiContoursBezier2D *courbes,
                                     double grain, UINT L, UINT H);

/* Affiche le nombre de points et courbes dans la structure (liste de listes de
 * béziers) */
void afficher_infos_bezier_2d (MultiContoursBezier2D *contours);
#endif /* _SIMPLIFICATION_H_ */