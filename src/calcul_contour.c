/*////////////////////////////////////////////////////////////////////////////

         /$$      /$$  /$$$$$$  /$$$$$$$  /$$   /$$  /$$$$$$    /$$
        | $$$    /$$$ /$$__  $$| $$__  $$| $$  | $$ /$$$_  $$ /$$$$
        | $$$$  /$$$$| $$  \ $$| $$  \ $$| $$  | $$| $$$$\ $$|_  $$
        | $$ $$/$$ $$| $$$$$$$$| $$$$$$$/| $$$$$$$$| $$ $$ $$  | $$
        | $$  $$$| $$| $$__  $$| $$____/ |_____  $$| $$\ $$$$  | $$
        | $$\  $ | $$| $$  | $$| $$            | $$| $$ \ $$$  | $$
        | $$ \/  | $$| $$  | $$| $$            | $$|  $$$$$$/ /$$$$$$
        |__/     |__/|__/  |__/|__/            |__/ \______/ |______/

/////////////////////////////////////////////////////////////////////////// */
// Fichier: calcul_contour.c
// Description: Implémentation de la détection de contours
///////////////////////////////////////////////////////////////////////////

#include "calcul_contour.h"
#include "image.h"
#include "types_macros.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// retourne le pixel à la gauche du robot
Pixel pixel_gauche (Image I, int x, int y, Orientation O) {

  Pixel res;

  switch (O) {
  case Nord:
    res = get_pixel_image (I, x, y);
    break;
  case Ouest:
    res = get_pixel_image (I, x, y + 1);
    break;
  case Sud:
    res = get_pixel_image (I, x + 1, y + 1);
    break;
  case Est:
    res = get_pixel_image (I, x + 1, y);
    break;
  default:
    ERREUR_FATALE ("Orientation non inclu dans l'enum");
    break;
  }

  return res;
}

// retourne le pixel à la droite du robot
Pixel pixel_droite (Image I, int x, int y, Orientation O) {

  Pixel res;

  switch (O) {
  case Nord:
    res = get_pixel_image (I, x + 1, y);
    break;
  case Ouest:
    res = get_pixel_image (I, x, y);
    break;
  case Sud:
    res = get_pixel_image (I, x, y + 1);
    break;
  case Est:
    res = get_pixel_image (I, x + 1, y + 1);
    break;
  default:
    ERREUR_FATALE ("Orientation non inclu dans l'enum");
    break;
  }

  return res;
}

// retourne la direction à gauche de celle spécifié
Orientation tourner_gauche (Orientation O) {
  switch (O) {
  case Nord:
    return Ouest;
    break;
  case Ouest:
    return Sud;
    break;
  case Sud:
    return Est;
    break;
  case Est:
    return Nord;
    break;
  default:
    ERREUR_FATALE ("Orientation non inclu dans l'enum");
    break;
  }
}

// retourne la direction à droite de celle spécifié
Orientation tourner_droite (Orientation O) {
  switch (O) {
  case Nord:
    return Est;
    break;
  case Ouest:
    return Nord;
    break;
  case Sud:
    return Ouest;
    break;
  case Est:
    return Sud;
    break;
  default:
    ERREUR_FATALE ("Orientation non inclu dans l'enum");
    break;
  }
}

// calcule la nouvelle orientation du robot qui parcours le contours
Orientation nouvelle_orientation (Image I, int x, int y, Orientation O) {

  // récupère les pixel à gauche et à droite pour s'orienter
  Pixel pixel_g = pixel_gauche (I, x, y, O);
  Pixel pixel_d = pixel_droite (I, x, y, O);

  // printf("nouvelle_orientation: %d, %d, %d, %d\n", x, y, pixel_g==NOIR,
  // pixel_d==NOIR); printf("orientation:%d\n", (int)O);

  // si on a un pixel noir à gauche, on tourne à gauche
  if (pixel_g == NOIR) {
    return tourner_gauche (O);
    // si on a un pixel blanc à droite, on tourne à droite
  } else if (pixel_d == BLANC) {
    return tourner_droite (O);
    // on renvoie une erreur si le robot n'est pas sur le contour
  } else if (pixel_d == NOIR) {
    return O;
    // on renvoie une erreur si le robot n'est pas sur le contour
  } else {
    ERREUR_FATALE ("Robot n'est pas sur contour");
    return O;
  }
}

// calcule un pixel de départ pour le calcul du contour
// todo: fonction nouveau_pixel_depart qui prends des
//              coutours déjà visités en argument
Coordonnees_pixel pixel_depart (Image I) {
  // on se positionne au début
  int x = 1, y = 1;
  // on récupère les dimensions de l'image
  UINT h = hauteur_image (I);
  UINT l = largeur_image (I);
  // et on initialise de quoi boucler
  Pixel pixel_actuel;
  Pixel pixel_nord;

  // on parcours les pixel jusqu'à en trouver un
  // qui est noir et a un voisin nord blanc
  for (int i = 1; i < h; i++) {
    for (int j = 1; j < l; j++) {
      pixel_actuel = get_pixel_image (I, j, i);
      pixel_nord = get_pixel_image (I, j, i - 1);
      if (pixel_actuel == NOIR && pixel_nord == BLANC) {
        return (Coordonnees_pixel){j, i};
      }
    }
  }

  // on quitte et affiche une erreur si rien trouvé
  ERREUR_FATALE ("Aucun pixel noir sur l'image");
  // return jamais éxécuté en théorie
  return (Coordonnees_pixel){x, y};
}

// retourne les coordonées du point suivant selon l'orientation
Coordonnees_point avancer (Coordonnees_point XY, Orientation O) {
  switch (O) {
  case Nord:
    return (Coordonnees_point){XY.x, XY.y - 1};
    break;
  case Ouest:
    return (Coordonnees_point){XY.x - 1, XY.y};
    break;
  case Sud:
    return (Coordonnees_point){XY.x, XY.y + 1};
    break;
  case Est:
    return (Coordonnees_point){XY.x + 1, XY.y};
    break;
  }
}

// algorithme principal qui affiche le contour
Image dessiner_contours (Image I) {
  // récupère les dimensions de l'image
  UINT h = hauteur_image (I);
  UINT l = largeur_image (I);

  // créer une image vide pour stocker le contour
  Image I2 = creer_image (l, h);

  // récupère un pixel de départ et la position du robot associée
  Coordonnees_pixel depart = pixel_depart (I);
  Coordonnees_point position_depart = {depart.x - 1, depart.y - 1};
  Coordonnees_point position = {depart.x - 1, depart.y - 1};
  Orientation O = Est;

  // arrêtera notre boucle une fois qu'on sera revenu au début
  bool pas_revenu = true;

  // printf("position initiale:%f, %f\n", position.x, position.y);

  // boucle principe du contour
  while (pas_revenu) {
    // memoriser passage
    dessiner_un_passage (I2, position);

    // execution d'un pas
    position = avancer (position, O);
    // printf("position_apres_avancer:%f, %f\n", position.x, position.y);
    O = nouvelle_orientation (I, position.x, position.y, O);

    // vérification qu'on ai pas fini le contours
    if (position.x == position_depart.x && position.y == position_depart.y &&
        O == Est) {
      pas_revenu = false;
    }
  }

  // appel à mémoriser le dernier point de contour
  dessiner_un_passage (I2, position);
  return I2;
}

// calcule le contours simple et le rentre dans une structure speciale
Contours *calculer_contours (Image I) {

  // alloue les deux entiers et le pointeur nécessaires pour la structure
  // Contours
  Contours *contours_image =
      malloc (sizeof (Coordonnees_point *) + sizeof (int) * 2);

  // si l'allocation a échouée
  if (contours_image == NULL || contours_image == 0x0) {
    // afficher l'erreur
    perror ("malloc: ");
    // on appelle notre macro qui gère les erreurs
    ERREUR_FATALE ("Erreur lors de l'allocation de la structure Contours.");
    return NULL;
  }

  // alloue l'espace initial du tableau
  contours_image->tab =
      malloc (CONTOURS_BLOC_SIZE * sizeof (Coordonnees_point));

  // si l'allocation a échouée
  if (contours_image->tab == NULL || contours_image->tab == 0x0) {
    // afficher l'erreur
    perror ("malloc: ");
    // on appelle notre macro qui gère les erreurs
    ERREUR_FATALE (
        "Erreur lors de l'allocation du tableau de la structure Contours.");
    return NULL;
  }

  // met la longueur à zéro et la longueur max à notre taille initiale
  contours_image->length = 0;
  contours_image->max_length = CONTOURS_BLOC_SIZE;

  // choisis le pixel de départ dans l'image I
  Coordonnees_pixel depart = pixel_depart (I);

  // on passe ces données en coordonnées de points
  Coordonnees_point position_depart = {depart.x - 1, depart.y - 1};
  Coordonnees_point position = {depart.x - 1, depart.y - 1};

  // l'orientation initiale du robot est à l'est
  Orientation O = Est;

  // arrêtera notre boucle une fois qu'on sera revenu au début
  bool pas_revenu = true;

  // boucle principe du contour
  while (pas_revenu) {
    // memoriser passage

    memoriser_passage (contours_image, position);

    // execution d'un pas
    position = avancer (position, O);

    // on calcule la prochaine orientation
    O = nouvelle_orientation (I, position.x, position.y, O);

    // vérification qu'on ai pas fini le contours en revenant au début
    if (position.x == position_depart.x && position.y == position_depart.y &&
        O == Est) {
      // si on a fini le contours, il faut arrêter
      pas_revenu = false;
    }
  }

  // on va bien stocker le dernier passage en mémoire
  memoriser_passage (contours_image, position);
  // on retourne notre contours
  return contours_image;
}

// affiche des informations sur la structure MultiContours donnée
void afficher_infos_contours (MultiContours *contours_image) {
  int nb_points = 0;
  int nb_segments = 0;
  int i;

  // pour chaque contours dans la structure
  for (i = 0; i < contours_image->length; i++) {
    // additionner le nombre de point
    nb_points += contours_image->contours[i].length;
    // additionner le nombre de segments
    nb_segments += contours_image->contours[i].length - 1;
  }

  // afficher les informations récoltées
  printf ("Nombre de contours: %d\n", contours_image->length);
  printf ("Nombre de points: %d\n", nb_points);
  printf ("Nombre de segments: %d\n", nb_segments);

  // fin de la fonction
  return;
}

// calcul TOUS les contours de l'image I
MultiContours *calculer_multi_contours (Image I) {

  // on alloue une structure pour stocker le pointeur vers le tableau et ses
  // tailles
  MultiContours *contours_image =
      malloc (sizeof (int) * 2 + sizeof (Contours *));
  // si l'allocation a échouée
  if (contours_image == NULL || contours_image == 0x0) {
    // afficher l'erreur
    perror ("malloc: ");
    // on appelle notre macro qui gère les erreurs
    ERREUR_FATALE (
        "Erreur lors de l'allocation de la structure MultiContours.");
    return NULL;
  }

  // et on alloue le tableau de contours (pointeurs vers des structures
  // Contours)
  contours_image->contours = malloc (NB_CONTOURS_BLOC_SIZE * sizeof (Contours));
  // si l'allocation a échouée
  if (contours_image->contours == NULL || contours_image->contours == 0x0) {
    // afficher l'erreur
    perror ("malloc: ");
    // on appelle notre macro qui gère les erreurs
    ERREUR_FATALE ("Erreur lors de l'allocation du tableau de la structure "
                   "MultiContours.");
    return NULL;
  }

  // on initialise la longueur par défaut et la longueur max
  contours_image->length = 1;
  contours_image->max_length = NB_CONTOURS_BLOC_SIZE;

  // on va ensuite initialiser le premier des contours dans le tableau
  // il a une taille encore nulle
  contours_image->contours[0].length = 0;
  // sa taille max par défaut est celle d'un de nos "blocks" de réallocation
  contours_image->contours[0].max_length = CONTOURS_BLOC_SIZE;
  // on alloue ensuite l'espace nécessaire pour le tableau de points
  contours_image->contours[0].tab =
      malloc (CONTOURS_BLOC_SIZE * sizeof (Coordonnees_point));
  // si l'allocation a échouée
  if (contours_image->contours[0].tab == NULL ||
      contours_image->contours[0].tab == 0x0) {
    // afficher l'erreur
    perror ("malloc: ");
    // on appelle notre macro qui gère les erreurs
    ERREUR_FATALE ("Erreur lors de l'allocation du tableau de la structure "
                   "Contours dans l'objet MultiContours.");
    return NULL;
  }

  // création du masque image qui va permettre de déterminer les contours encore
  // non parcourus
  Image matrice_contours = initialiser_matrice_image (I);

  // ceci est la booléenne qui va s'activer si on reviens au départ
  bool pas_revenu = true;

  // tant que notre masque comporte des pixels noirs
  // (ie pour chaque contour trouvé sur l'image)
  while (premier_pixel_noir (matrice_contours).x != -1) {

    // récupérer le premier pixel noir
    Coordonnees_pixel depart = premier_pixel_noir (matrice_contours);

    // on va ensuite enregistrer les coordonnées de départ (pixels) en coords de
    // points
    Coordonnees_point position_depart = {depart.x - 1, depart.y - 1};
    Coordonnees_point position = {depart.x - 1, depart.y - 1};
    // et on met le pixel sur lequel on passe à "blanc" sur le masque
    set_pixel_image (matrice_contours, depart.x, depart.y, BLANC);
    Orientation O = Est;

    // tant que le robot n'est pas revenu à sa position de départ
    pas_revenu = true;
    // pour chaque point du contour actuel
    while (pas_revenu) {

      // memoriser passage
      memoriser_passage (&contours_image->contours[contours_image->length - 1],
                         position);

      // si on se déplace à l'est, marquer le voisin SE comme faisant parti d'un
      // contours
      if (O == Est) {
        // on met le pixel SUD EST à "blanc" sur le masque
        set_pixel_image (matrice_contours, position.x + 1, position.y + 1,
                         BLANC);
      }

      // execution d'un pas
      position = avancer (position, O);
      // printf("position_apres_avancer:%f, %f\n", position.x, position.y);
      O = nouvelle_orientation (I, position.x, position.y, O);

      // vérification qu'on ai pas fini le contours
      if (position.x == position_depart.x && position.y == position_depart.y &&
          O == Est) {
        pas_revenu = false;
      }
    }

    // mémoriser le dernier passage
    memoriser_passage (&contours_image->contours[contours_image->length - 1],
                       position);

    // si on se déplace à l'est, marquer le voisin SE comme faisant parti d'un
    // contours
    set_pixel_image (matrice_contours, position.x + 1, position.y + 1, BLANC);

    // si notre tableau est à sa taille maximum
    if (contours_image->length >= contours_image->max_length) {
      // calcul de la nouvelle taille
      int nouv_taille = (contours_image->max_length + NB_CONTOURS_BLOC_SIZE) *
                        (sizeof (Contours));
      // on test l'allocation d'une nouvelle zone memoire
      Contours *nouvelle_alloc =
          realloc (contours_image->contours, nouv_taille);
      // on vérifie si l'allocation n'a pas échoué
      if (nouvelle_alloc == NULL || nouvelle_alloc == 0x0) {
        perror ("realloc: ");
        ERREUR_FATALE ("Erreur lors de la réallocation d'un tableau.");
        return NULL;
      } else {
        // réassigne la nouvelle zone réservée
        contours_image->contours = nouvelle_alloc;
        // incrémentation de la taille max
        contours_image->max_length =
            contours_image->max_length + NB_CONTOURS_BLOC_SIZE;
      }
    }

    // incrémenter pour passer au contour suivant
    contours_image->length = contours_image->length + 1;
    // va déclencher une allocation dans memoriser_passage
    contours_image->contours[contours_image->length - 1].length = 0;
    contours_image->contours[contours_image->length - 1].max_length =
        CONTOURS_BLOC_SIZE;
    // on met le tableau à NULL, et on ne lui allouera que si besoin
    contours_image->contours[contours_image->length - 1].tab =
        malloc (CONTOURS_BLOC_SIZE * sizeof (Coordonnees_point));
    // si l'allocation a échouée
    if (contours_image->contours[contours_image->length - 1].tab == NULL ||
        contours_image->contours[contours_image->length - 1].tab == 0x0) {
      // afficher l'erreur
      perror ("malloc: ");
      // on appelle notre macro qui gère les erreurs
      ERREUR_FATALE ("Erreur lors de l'allocation du tableau de la structure "
                     "Contours dans l'objet MultiContours (2).");
      return NULL;
    }
  }

  // le dernier contours ne sert jamais, donc on l'enlève
  free (contours_image->contours[contours_image->length - 1].tab);
  free (matrice_contours.tab);
  contours_image->length = contours_image->length - 1;

  return contours_image;
}

void memoriser_passage (Contours *contours_image, Coordonnees_point position) {
  // si la taille est égale à la taille max
  if (contours_image->length >= contours_image->max_length) {
    // on essaye de réallouer une taille de bloc mémoire plus grand
    Coordonnees_point *nouvelle_alloc = realloc (
        contours_image->tab, (contours_image->max_length + CONTOURS_BLOC_SIZE) *
                                 sizeof (Coordonnees_point));
    // si l'allocation a échouée
    if (nouvelle_alloc == NULL || nouvelle_alloc == 0x0) {
      // afficher l'erreur
      perror ("realloc: ");
      // on appelle notre macro qui gère les erreurs
      ERREUR_FATALE ("Erreur lors de la réallocation d'un tableau.");
      return;
    } else {
      // si notre allocation a bien marché
      // on fait pointer notre tableau vers la potentielle nouvelle addresse
      contours_image->tab = nouvelle_alloc;
      // et on met à jour la taille max
      contours_image->max_length =
          contours_image->max_length + CONTOURS_BLOC_SIZE;
    }
  }

  // on oublie pas d'incrémenter la taille
  contours_image->length++;
  // une fois qu'on est sur d'avoir de la place
  // on ajoute la nouvelle position au contour
  contours_image->tab[contours_image->length - 1].x = position.x;
  contours_image->tab[contours_image->length - 1].y = position.y;

  // et la fonction se termine ici
  return;
}

Contours *segment_contours (Contours *mon_contour, int j1, int j2) {
  // alloue les deux entiers et le pointeur nécessaires pour la structure
  // Contours
  Contours *nouveau_segment =
      malloc (sizeof (Coordonnees_point *) + sizeof (int) * 2);

  // si l'allocation a échouée
  if (nouveau_segment == NULL || nouveau_segment == 0x0) {
    // afficher l'erreur
    perror ("malloc: ");
    // on appelle notre macro qui gère les erreurs
    ERREUR_FATALE ("Erreur lors de l'allocation de la structure Contours.");
    return NULL;
  }

  // alloue l'espace initial du tableau
  nouveau_segment->tab =
      malloc (CONTOURS_BLOC_SIZE * sizeof (Coordonnees_point));

  // si l'allocation a échouée
  if (nouveau_segment->tab == NULL || nouveau_segment->tab == 0x0) {
    // afficher l'erreur
    perror ("malloc: ");
    // on appelle notre macro qui gère les erreurs
    ERREUR_FATALE (
        "Erreur lors de l'allocation du tableau de la structure Contours.");
    return NULL;
  }

  // met la longueur à zéro et la longueur max à notre taille initiale
  nouveau_segment->length = 2;
  nouveau_segment->max_length = CONTOURS_BLOC_SIZE;

  nouveau_segment->tab[0].x = mon_contour->tab[j1].x;
  nouveau_segment->tab[0].y = mon_contour->tab[j1].y;

  nouveau_segment->tab[1].x = mon_contour->tab[j2].x;
  nouveau_segment->tab[1].y = mon_contour->tab[j2].y;

  return nouveau_segment;
}

void concatener_contours (Contours *L1, Contours *L2) {
  // on teste tout d'abord si les chemins peuvent être mis bout à bouts
  if (L1->tab[L1->length - 1].x != L2->tab[0].x ||
      L1->tab[L1->length - 1].y != L2->tab[0].y) {
    ERREUR_FATALE ("Erreur, vous essayer de concatener deux chemins qui ne "
                   "sont pas liées.");
    return;
  }

  // si tout est ok
  int i;
  // pour chaque point de L2
  for (i = 0; i < L2->length - 1; i++) {
    // memoriser le point de L2 dans L1
    memoriser_passage (L1, L2->tab[i + 1]);
  }

  free (L2->tab);
  free (L2);

  return;
}

// crée un masque qui marque en noir chaque pixel noir avec un voisin nord blanc
// algorithme vu en classe
Image initialiser_matrice_image (Image I) {
  // on récupère la largeur et hauteur de l'image
  int width = largeur_image (I);
  int height = hauteur_image (I);
  // on créer une image masque avec ces dimensions
  Image mask = creer_image (width, height);
  // on créer des pixels qui nous servent pour les calculs
  Pixel current, north_neighbour;

  // pour chaque pixel de l'image
  for (int i = 0; i < height; i++) {
    for (int j = 0; j < width; j++) {
      // on récupère le pixel courant (+1 car on va de 1 à max en réalité)
      current = get_pixel_image (I, j + 1, i + 1);
      // on récupère le voisin Nord
      north_neighbour = get_pixel_image (I, j + 1, i);
      // si le pixel sur leuquel on est est noir
      // et que son voisin nord est blanc
      if (current == NOIR && north_neighbour == BLANC) {
        // on marque ce pixel comme borne d'un contour
        set_pixel_image (mask, j + 1, i + 1, NOIR);
      } else {
        // ou on le supprimer des points du masque
        set_pixel_image (mask, j + 1, i + 1, BLANC);
      }
    }
  }

  // on retourne l'image masque nouvellement crée
  return mask;
}

// fonction qui cherche le premier pixel noir de l'image
Coordonnees_pixel premier_pixel_noir (Image I) {
  // récupère les dimensions de l'image
  int width = largeur_image (I);
  int height = hauteur_image (I);
  // valeurs qui servent pour le calcul
  Coordonnees_pixel pixel_noir;
  Pixel current;

  // parcourir l'image de gauche à droite et de haut en bas
  for (int i = 0; i < height; i++) {
    for (int j = 0; j < width; j++) {
      // récupère la couleur du pixel
      current = get_pixel_image (I, j + 1, i + 1);
      if (current == NOIR) {
        // si pixel est noir, on retourne ses coordonneés
        pixel_noir.x = j + 1;
        pixel_noir.y = i + 1;
        return pixel_noir;
      }
    }
  }

  // si on ne trouve rien, retourner notre signal secret de la mort
  pixel_noir.x = -1;
  pixel_noir.y = -1;
  return pixel_noir;
}

// fonction qui mémorise le passage sur un contour dans une structure en mémoire
void dessiner_un_passage (Image I, Coordonnees_point position) {
  set_pixel_image (I, position.x, position.y, NOIR);
}

int ecrire_contour (Contours *mon_contour, char chemin[]) {
  FILE *destination;

  destination = fopen (chemin, "w+");
  if (destination == NULL) {
    perror ("ecrire_contours");
    ERREUR_FATALE ("Erreur");
    return 1;
  }

  fprintf (destination, "1\n");

  fprintf (destination, "%d\n", mon_contour->length);

  for (int i = 0; i < mon_contour->length; i++) {
    fprintf (destination, "%f %f\n", mon_contour->tab[i].x,
             mon_contour->tab[i].y);
  }

  fclose (destination);

  return 0;
}

int ecrire_multi_contour (MultiContours *mon_contour, char chemin[]) {
  FILE *destination;

  destination = fopen (chemin, "w+");
  if (destination == NULL) {
    perror ("ecrire_contours");
    ERREUR_FATALE ("Erreur");
    return 1;
  }

  fprintf (destination, "%d\n", mon_contour->length);

  int j;
  for (j = 0; j < mon_contour->length; j++) {

    fprintf (destination, "%d\n", mon_contour->contours[j].length);

    for (int i = 0; i < mon_contour->contours[j].length; i++) {
      fprintf (destination, "%f %f\n", mon_contour->contours[j].tab[i].x,
               mon_contour->contours[j].tab[i].y);
    }
  }

  fclose (destination);

  return 0;
}

int ecrire_contour_eps_mode_1 (Contours *mon_contour, char chemin[], UINT l,
                               UINT h) {
  FILE *destination;

  destination = fopen (chemin, "w+");
  if (destination == NULL) {
    perror ("ecrire_contours");
    ERREUR_FATALE ("Erreur");
    return 1;
  }

  fprintf (destination, "%!PS-Adobe-3.0 EPSF-3.0\n");

  fprintf (destination, "%%%%BoundingBox: 0 0 %d %d\n", l, h);

  fprintf (destination, "%f %f moveto ", mon_contour->tab[0].x,
           (h - mon_contour->tab[0].y));

  for (int i = 1; i < mon_contour->length; i++) {
    fprintf (destination, "%f %f lineto ", mon_contour->tab[i].x,
             (h - mon_contour->tab[i].y));
  }

  fprintf (destination, "\nstroke\nshowpage");

  fclose (destination);

  return 0;
}

int ecrire_contour_eps_mode_2 (Contours *mon_contour, char chemin[], UINT l,
                               UINT h) {
  FILE *destination;

  destination = fopen (chemin, "w+");
  if (destination == NULL) {
    perror ("ecrire_contours");
    ERREUR_FATALE ("Erreur");
    return 1;
  }

  fprintf (destination, "%!PS-Adobe-3.0 EPSF-3.0\n");

  fprintf (destination, "%%%%BoundingBox: 0 0 %d %d\n", l, h);

  fprintf (destination, "%f %f moveto ", mon_contour->tab[0].x,
           (h - mon_contour->tab[0].y));

  for (int i = 1; i < mon_contour->length; i++) {
    fprintf (destination, "%f %f lineto ", mon_contour->tab[i].x,
             (h - mon_contour->tab[i].y));
  }

  fprintf (destination, "\n0.05 setlinewidth stroke\n");

  for (int i = 0; i < mon_contour->length; i++) {
    fprintf (destination, "newpath %f %f 0.2 0 360 arc fill closepath ",
             mon_contour->tab[i].x, (h - mon_contour->tab[i].y));
  }

  fprintf (destination, "showpage\n");

  fclose (destination);

  return 0;
}

int ecrire_contour_eps_mode_3 (Contours *mon_contour, char chemin[], UINT l,
                               UINT h) {
  FILE *destination;

  destination = fopen (chemin, "w+");
  if (destination == NULL) {
    perror ("ecrire_contours");
    ERREUR_FATALE ("Erreur");
    return 1;
  }

  fprintf (destination, "%!PS-Adobe-3.0 EPSF-3.0\n");

  fprintf (destination, "%%%%BoundingBox: 0 0 %d %d\n", l, h);

  fprintf (destination, "%f %f moveto ", mon_contour->tab[0].x,
           (h - mon_contour->tab[0].y));

  for (int i = 1; i < mon_contour->length; i++) {
    fprintf (destination, "%f %f lineto ", mon_contour->tab[i].x,
             (h - mon_contour->tab[i].y));
  }

  fprintf (destination, "\nfill \n");

  fprintf (destination, "showpage\n");

  fclose (destination);

  return 0;
}

void ecrire_multi_contour_eps_mode_3 (MultiContours *mon_contour, char chemin[],
                                      UINT l, UINT h) {
  FILE *destination;

  destination = fopen (chemin, "w+");
  if (destination == NULL) {
    perror ("ecrire_contours");
    ERREUR_FATALE ("Erreur");
    return;
  }

  fprintf (destination, "%!PS-Adobe-3.0 EPSF-3.0\n");

  fprintf (destination, "%%%%BoundingBox: 0 0 %d %d\n", l, h);

  int j;
  for (j = 0; j < mon_contour->length; j++) {

    // instruction a ecrire pour chaque contour
    fprintf (destination, "%f %f moveto ", mon_contour->contours[j].tab[0].x,
             (h - mon_contour->contours[j].tab[0].y));
    // fprintf(destination, "%d\n", mon_contour->contours[j].length);

    for (int i = 0; i < mon_contour->contours[j].length; i++) {
      // instruction à écrire pour chaque point
      fprintf (destination, "%f %f lineto ", mon_contour->contours[j].tab[i].x,
               (h - mon_contour->contours[j].tab[i].y));
      // fprintf(destination, "%f %f\n", mon_contour->contours[j].tab[i].x,
      // mon_contour->contours[j].tab[i].y);
    }
  }

  fprintf (destination, "\nfill \n");

  fprintf (destination, "showpage\n");

  fclose (destination);

  return;
}

void free_contours (Contours *contours) {
  free (contours->tab);
  free (contours);
}

void free_multi_contours (MultiContours *multi_contours) {
  int i;
  for (i = 0; i < multi_contours->max_length; i++) {
    free_contours (&multi_contours->contours[i]);
  }
  free (multi_contours);
}
