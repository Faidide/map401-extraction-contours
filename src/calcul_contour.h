/*////////////////////////////////////////////////////////////////////////////

         /$$      /$$  /$$$$$$  /$$$$$$$  /$$   /$$  /$$$$$$    /$$
        | $$$    /$$$ /$$__  $$| $$__  $$| $$  | $$ /$$$_  $$ /$$$$
        | $$$$  /$$$$| $$  \ $$| $$  \ $$| $$  | $$| $$$$\ $$|_  $$
        | $$ $$/$$ $$| $$$$$$$$| $$$$$$$/| $$$$$$$$| $$ $$ $$  | $$
        | $$  $$$| $$| $$__  $$| $$____/ |_____  $$| $$\ $$$$  | $$
        | $$\  $ | $$| $$  | $$| $$            | $$| $$ \ $$$  | $$
        | $$ \/  | $$| $$  | $$| $$            | $$|  $$$$$$/ /$$$$$$
        |__/     |__/|__/  |__/|__/            |__/ \______/ |______/

/////////////////////////////////////////////////////////////////////////// */
// Fichier: calcul_contour.h
// Description: Headers de la détection de contours
///////////////////////////////////////////////////////////////////////////

#ifndef _CALCUL_CONTOUR_H_
#define _CALCUL_CONTOUR_H_
#include "image.h"

#define CONTOURS_BLOC_SIZE 3000
#define NB_CONTOURS_BLOC_SIZE 20

typedef enum { Nord, Est, Sud, Ouest } Orientation;

typedef struct Coordonnees_pixel_ {
  int x, y;
} Coordonnees_pixel;

typedef struct Coordonnees_point_ {
  double x, y;
} Coordonnees_point;

typedef struct Contours_ {
  Coordonnees_point *tab;
  int length;
  int max_length;
} Contours;

typedef struct MultiContours_ {
  Contours *contours;
  int length;
  int max_length;
} MultiContours;

Pixel pixel_gauche (Image I, int x, int y, Orientation O);

Pixel pixel_droite (Image I, int x, int y, Orientation O);

Orientation tourner_gauche (Orientation O);

Orientation tourner_droite (Orientation O);

Orientation nouvelle_orientation (Image I, int x, int y, Orientation O);

Coordonnees_pixel pixel_depart (Image I);

void dessiner_un_passage (Image I, Coordonnees_point position);

Image dessiner_contours (Image I);

int ecrire_contour (Contours *mon_contour, char chemin[]);

int ecrire_multi_contour (MultiContours *mon_contour, char chemin[]);

void memoriser_passage (Contours *contours_image, Coordonnees_point position);

Contours *calculer_contours (Image I);

MultiContours *calculer_multi_contours (Image I);

void free_contours (Contours *contours);

void free_multi_contours (MultiContours *multi_contours);

int ecrire_contour_eps_mode_1 (Contours *mon_contour, char chemin[], UINT l,
                               UINT h);

int ecrire_contour_eps_mode_2 (Contours *mon_contour, char chemin[], UINT l,
                               UINT h);

int ecrire_contour_eps_mode_3 (Contours *mon_contour, char chemin[], UINT l,
                               UINT h);

Image initialiser_matrice_image (Image I);

Coordonnees_pixel premier_pixel_noir (Image I);

void afficher_infos_contours (MultiContours *contours_image);

void ecrire_multi_contour_eps_mode_3 (MultiContours *contours_image,
                                      char chemin[], UINT l, UINT h);

Contours *segment_contours (Contours *mon_contour, int j1, int j2);

void concatener_contours (Contours *L1, Contours *L2);

#endif