/*////////////////////////////////////////////////////////////////////////////

         /$$      /$$  /$$$$$$  /$$$$$$$  /$$   /$$  /$$$$$$    /$$
        | $$$    /$$$ /$$__  $$| $$__  $$| $$  | $$ /$$$_  $$ /$$$$
        | $$$$  /$$$$| $$  \ $$| $$  \ $$| $$  | $$| $$$$\ $$|_  $$
        | $$ $$/$$ $$| $$$$$$$$| $$$$$$$/| $$$$$$$$| $$ $$ $$  | $$
        | $$  $$$| $$| $$__  $$| $$____/ |_____  $$| $$\ $$$$  | $$
        | $$\  $ | $$| $$  | $$| $$            | $$| $$ \ $$$  | $$
        | $$ \/  | $$| $$  | $$| $$            | $$|  $$$$$$/ /$$$$$$
        |__/     |__/|__/  |__/|__/            |__/ \______/ |______/

/////////////////////////////////////////////////////////////////////////// */
// Fichier: geom2d.c
// Description: Implémentation de la geometrie 2D
///////////////////////////////////////////////////////////////////////////

#include "geom2d.h"
#include <math.h>
#include <stdio.h>

/* Norme d'un vecteur V */
double norme_vecteur (Vecteur V) { return sqrt (V.x * V.x + V.y * V.y); }

/* Produit scalaire de deux vecteurs avec des coordonnées de points */
double produit_scalaire (Point A, Point B) { return (A.x * B.x + A.y * B.y); }

/* Cree le point de coordonnées (x,y) */
Point set_point (double x, double y) {
  Point P = {x, y};
  return P;
}

/* Somme de deux points P1 et P2 */
Point add_point (Point P1, Point P2) {
  return set_point (P1.x + P2.x, P1.y + P2.y);
}

/* Vecteur correspondant au bipoint AB */
Vecteur vect_bipoint (Point A, Point B) {
  Vecteur V = {B.x - A.x, B.y - A.y};
  return V;
}

/* Somme de deux vecteur V1 et V2 */
Vecteur add_vect (Vecteur V1, Vecteur V2) {
  return (Vecteur){V1.x + V2.x, V1.y + V2.y};
}

/* Multiplication d'un vecteur V avec un scalaire r */
Vecteur multiplication_scalaire_vect (Vecteur V, double r) {
  return (Vecteur){V.x * r, V.y * r};
}

/* Distance entre deux point A et B */
double distance (Point A, Point B) {
  return norme_vecteur (vect_bipoint (A, B));
}

double longueur_carree (Point A, Point B) {
  return (B.x - A.x) * (B.x - A.x) + (B.y - A.y) * (B.y - A.y);
}

/* Distance entre le point C et la droite formée par A et B */
double distance_point_segment (Point A, Point B, Point C) {
  // on calcule la distance carrée
  double l2 = longueur_carree (A, B);
  // si la distance est nulle, les points sont confondus
  // et on peut directement retourner le résultat avec ceci
  if (l2 == 0.0)
    return distance (A, C);

  // il nous fait P1 et P2, car leur produit scalaire divisé
  // par l2 nous donne le t de la projection orthogonale de C sur [AB]
  // calculer C - A
  Point P1 = {C.x - A.x, C.y - A.y};

  // calcluer B - A
  Point P2 = {B.x - A.x, B.y - A.y};

  // calcul du t sur lequel trouver la projection
  double t = fmax (0.0f, fmin (1, produit_scalaire (P1, P2) / l2));
  // calcul du projetté orthogonal de C sur [AB]
  Point projection = {A.x + t * (B.x - A.x), A.y + t * (B.y - A.y)};
  // retourne la distance entre la projection et le point C
  return distance (C, projection);
}
