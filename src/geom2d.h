/*////////////////////////////////////////////////////////////////////////////

         /$$      /$$  /$$$$$$  /$$$$$$$  /$$   /$$  /$$$$$$    /$$
        | $$$    /$$$ /$$__  $$| $$__  $$| $$  | $$ /$$$_  $$ /$$$$
        | $$$$  /$$$$| $$  \ $$| $$  \ $$| $$  | $$| $$$$\ $$|_  $$
        | $$ $$/$$ $$| $$$$$$$$| $$$$$$$/| $$$$$$$$| $$ $$ $$  | $$
        | $$  $$$| $$| $$__  $$| $$____/ |_____  $$| $$\ $$$$  | $$
        | $$\  $ | $$| $$  | $$| $$            | $$| $$ \ $$$  | $$
        | $$ \/  | $$| $$  | $$| $$            | $$|  $$$$$$/ /$$$$$$
        |__/     |__/|__/  |__/|__/            |__/ \______/ |______/

/////////////////////////////////////////////////////////////////////////// */
// Fichier: geom2d.h
// Description: Descriptions des fonctions de la geometrie 2D
///////////////////////////////////////////////////////////////////////////

#ifndef _GEOM2D_H_
#define _GEOM2D_H_

typedef struct Vecteur_ {
  double x, y;
} Vecteur;

typedef struct Point_ {
  double x, y;
} Point;

/* Cree le point de coordonn�es (x,y) */
Point set_point (double x, double y);

/* Somme de deux points P1 et P2 */
Point add_point (Point P1, Point P2);

/* Vecteur correspondant au bipoint AB */
Vecteur vect_bipoint (Point A, Point B);

/* Somme de deux vecteur V1 et V2 */
Vecteur add_vect (Vecteur V1, Vecteur V2);

/* Multiplication d'un vecteur V avec un scalaire r */
Vecteur multiplication_scalaire_vect (Vecteur V, double r);

/* Norme d'un vecteur V */
double norme_vecteur (Vecteur V);

/* Distance entre deux point A et B */
double distance (Point A, Point B);

/* distance au carré entre deux points */
double longueur_carree (Point A, Point B);

/* Distance entre le point C et la droite formée par A et B */
double distance_point_segment (Point A, Point B, Point C);

/* Produit scalaire de deux vecteurs avec des coordonnées de points */
double produit_scalaire (Point A, Point B);

#endif
