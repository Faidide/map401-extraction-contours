#include "simplification.h"
#include "geom2d.h"


/* simplifier le contours avec un distance d (recursif) */
Contours * simplifier_contour_douglas_peucker (Contours * mon_contour, int j1, int j2, double distance) {
  // static pour éviter de surcharger la frame avec des variables locales à chaque appel de la fonction
  double dmax = 0.0f;
  double dj = 0.0f;
  int j;
  int k = j1;

  for(j= j1+1 ; j<=j2; j++) {
    // distance point-segment entre le point j et la ligne [j1,j2]
    dj = distance_point_segment ((Point){ mon_contour->tab[j1].x, mon_contour->tab[j1].y },
                                 (Point){ mon_contour->tab[j2].x, mon_contour->tab[j2].y },
                                 (Point){ mon_contour->tab[j].x, mon_contour->tab[j].y  });
    
    // si la distance est la plus grand jamais vue, la sauvegarder
    if( dmax < dj ) {
      dmax = dj;
      k = j;
    }
  }

  // si notre plus grande distance ne dépasse pas d
  if( dmax <= distance ) {
    // on simplifie notre contour en un unique segment entre j1 et j2
    return segment_contours(mon_contour, j1, j2);
  // sinon, dmax est plus grand que d
  } else {
    // on va diviser notre tâche en deux
    // calcul du contour simplifié gauche
    Contours * L1 = simplifier_contour_douglas_peucker(mon_contour, j1, k, distance);
    // calcul du contour simplifié droit
    Contours * L2 = simplifier_contour_douglas_peucker(mon_contour, k, j2, distance);

    // on concatene L2 dans L1
    concatener_contours(L1,L2);

    // on retourne la concaténation des deux
    return L1;
  }
}

MultiContours * simplification_multi_contours (MultiContours * mes_contours, double distance) {
      
  // on alloue une structure pour stocker le pointeur vers le tableau et ses tailles
  MultiContours * contours_image = malloc( sizeof(int)*2 + sizeof(Contours*));
  // si l'allocation a échouée
  if(contours_image==NULL || contours_image==0x0) {
    // afficher l'erreur
    perror("malloc: ");
    // on appelle notre macro qui gère les erreurs
    ERREUR_FATALE("Erreur lors de l'allocation de la structure MultiContours.");
    return NULL;
  }
  
  // et on alloue le tableau de contours (pointeurs vers des structures Contours)
  contours_image->contours = malloc( mes_contours->max_length*sizeof(Contours) );
  // si l'allocation a échouée
  if(contours_image->contours==NULL || contours_image->contours==0x0) {
    // afficher l'erreur
    perror("malloc: ");
    // on appelle notre macro qui gère les erreurs
    ERREUR_FATALE("Erreur lors de l'allocation du tableau de la structure MultiContours.");
    return NULL;
  }

  // on initialise la longueur par défaut et la longueur max
  contours_image->length = mes_contours->length;
  contours_image->max_length = mes_contours->max_length;

  /////////////////////////////////
  // SIMPLIFICATION DES CONTOURS //
  /////////////////////////////////
  int i, j;
  // pour chaque contour
  for(i=0;i<mes_contours->length;i++) {
    // en générer un nouveau avec notre fonction de simplification
    Contours * nouveau_contours = simplifier_contour_douglas_peucker (&mes_contours->contours[i], 0, mes_contours->contours[i].length-1, distance);

    // si notre tableau est à sa taille maximum
    if (contours_image->length >= contours_image->max_length) {
      // calcul de la nouvelle taille
      int nouv_taille = (contours_image->max_length+NB_CONTOURS_BLOC_SIZE)*(sizeof(Contours));
      // on test l'allocation d'une nouvelle zone memoire
      Contours * nouvelle_alloc = realloc (contours_image->contours, nouv_taille);
      // on vérifie si l'allocation n'a pas échoué
      if( nouvelle_alloc==NULL || nouvelle_alloc==0x0) {
        perror("realloc: "); 
        ERREUR_FATALE("Erreur lors de la réallocation d'un tableau.");
        return NULL;
      } else {
        // réassigne la nouvelle zone réservée
        contours_image->contours = nouvelle_alloc;
        // incrémentation de la taille max
        contours_image->max_length = contours_image->max_length + NB_CONTOURS_BLOC_SIZE;
      }
    }

    // et on ajoute le contours crée à la structure MultiContours

    // on initialise le contours i
    // il a une taille encore nulle
    contours_image->contours[i].length = 0;
    // sa taille max par défaut est celle d'un de nos "blocks" de réallocation
    contours_image->contours[i].max_length = CONTOURS_BLOC_SIZE;
    // on alloue ensuite l'espace nécessaire pour le tableau de points
    contours_image->contours[i].tab =  malloc(CONTOURS_BLOC_SIZE*sizeof(Coordonnees_point));
    // si l'allocation a échouée
    if(contours_image->contours[i].tab==NULL || contours_image->contours[i].tab==0x0) {
        // afficher l'erreur
        perror("malloc: ");
        // on appelle notre macro qui gère les erreurs
        ERREUR_FATALE("Erreur lors de l'allocation du tableau de la structure Contours dans l'objet MultiContours.");
        return NULL;
    }


    // pour chaque point du contours simplifié
    for(j=0;j<nouveau_contours->length;j++) {
      // le memoriser dans la nouvelle structure MultiContours
      memoriser_passage(&contours_image->contours[i], nouveau_contours->tab[j]);
    }

    free(nouveau_contours->tab);
    free(nouveau_contours);
  }

  return contours_image;
}