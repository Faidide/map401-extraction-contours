/*////////////////////////////////////////////////////////////////////////////

         /$$      /$$  /$$$$$$  /$$$$$$$  /$$   /$$  /$$$$$$    /$$
        | $$$    /$$$ /$$__  $$| $$__  $$| $$  | $$ /$$$_  $$ /$$$$
        | $$$$  /$$$$| $$  \ $$| $$  \ $$| $$  | $$| $$$$\ $$|_  $$
        | $$ $$/$$ $$| $$$$$$$$| $$$$$$$/| $$$$$$$$| $$ $$ $$  | $$
        | $$  $$$| $$| $$__  $$| $$____/ |_____  $$| $$\ $$$$  | $$
        | $$\  $ | $$| $$  | $$| $$            | $$| $$ \ $$$  | $$
        | $$ \/  | $$| $$  | $$| $$            | $$|  $$$$$$/ /$$$$$$
        |__/     |__/|__/  |__/|__/            |__/ \______/ |______/

/////////////////////////////////////////////////////////////////////////// */
// Fichier: simplification.h
// Description: Descriptions des fonctions de simplification de contours
///////////////////////////////////////////////////////////////////////////

#ifndef _SIMPLIFICATION_H_
#define _SIMPLIFICATION_H_

#include "calcul_contour.h"
#include "types_macros.h"

Contours *simplifier_contour_douglas_peucker (Contours *mon_contour, int j1,
                                              int j2, double distance);

MultiContours *simplification_multi_contours (MultiContours *contours_structure,
                                              double distance);

#endif /* _SIMPLIFICATION_H_ */
