/*////////////////////////////////////////////////////////////////////////////

         /$$      /$$  /$$$$$$  /$$$$$$$  /$$   /$$  /$$$$$$    /$$
        | $$$    /$$$ /$$__  $$| $$__  $$| $$  | $$ /$$$_  $$ /$$$$
        | $$$$  /$$$$| $$  \ $$| $$  \ $$| $$  | $$| $$$$\ $$|_  $$
        | $$ $$/$$ $$| $$$$$$$$| $$$$$$$/| $$$$$$$$| $$ $$ $$  | $$
        | $$  $$$| $$| $$__  $$| $$____/ |_____  $$| $$\ $$$$  | $$
        | $$\  $ | $$| $$  | $$| $$            | $$| $$ \ $$$  | $$
        | $$ \/  | $$| $$  | $$| $$            | $$|  $$$$$$/ /$$$$$$
        |__/     |__/|__/  |__/|__/            |__/ \______/ |______/

/////////////////////////////////////////////////////////////////////////// */
// Fichier: test_bezier_simplification.c
// Description: Implémentation des tests témoins
///////////////////////////////////////////////////////////////////////////

#include "image.h"
#include "bezier.h"
#include "calcul_contour.h"
#include <stdio.h>
#include <string.h>

int main (int argc, char *argv[]) {
  if (argc != 2) {
    printf ("usage: %s [image.pbm]\n", argv[0]);
    return 0;
  }

  printf ("Lancement du prog de test!\n");

  // lecture de l'image à tester
  Image test_image = lire_fichier_image (argv[1]);

  // affichage de l'image à tester
  printf ("Affichage initial de l'image.\n");
  ecrire_image (test_image);

  // lancement du calcul de contours qui va le sauvarger en memoire
  printf ("Calcul du multi contours avec sauvegarde en memoire.\n");
  MultiContours *contours_structure = calculer_multi_contours (test_image);
  afficher_infos_contours (contours_structure);

  // simplifier le contours en bezier (pour d=1 et d=2)
  MultiContoursBezier2D * contours_bezier_d1 = simplification_bezier_multi_2D (contours_structure, 1.0);
  MultiContoursBezier2D * contours_bezier_d2 = simplification_bezier_multi_2D (contours_structure, 3.0);
  MultiContoursBezier2D * contours_bezier_d3 = simplification_bezier_multi_2D (contours_structure, 10.0);
  MultiContoursBezier2D * contours_bezier_d4 = simplification_bezier_multi_2D (contours_structure, 30.0);

  // generation des fichiers eps
  char suffix[20] = ".bezier_1_.eps";
  char base[256];

  // on récupère les dimensions de l'image
  UINT h = hauteur_image (test_image);
  UINT l = largeur_image (test_image);

  // ecriture du fichier eps du contours sans les béziers
  suffix[8] = '0';
  strcpy (base, argv[1]);
  strcat (base, suffix);

  ecrire_multi_contour_eps_mode_3 (contours_structure, base, l, h);

  // on prépare le nom de fichier
  suffix[8] = '1';
  strcpy (base, argv[1]);
  strcat (base, suffix);
  // ecriture de l'eps pour le coutours d=1 en mode remplissage
  ecrire_bezier_eps_2d (contours_bezier_d1, base, l, h);
  printf ("Simplification du contour (d=1.0) et affichage du résumé...\n");
  afficher_infos_bezier_2d (contours_bezier_d1);

  // simplification du contour d=3
  printf ("Simplification du contour (d=3.0) et affichage du résumé...\n");
  afficher_infos_bezier_2d (contours_bezier_d2);
  // on prépare le nom de fichier
  suffix[8] = '3';
  strcpy (base, argv[1]);
  strcat (base, suffix);
  // ecriture de l'eps pour le coutours d=3 en mode remplissage
  ecrire_bezier_eps_2d (contours_bezier_d2, base, l, h);

  // simplification du contour d=10
  printf ("Simplification du contour (d=10.0) et affichage du résumé...\n");
  afficher_infos_bezier_2d (contours_bezier_d3);
  // on prépare le nom de fichier
  suffix[8] = '1';
  suffix[9] = '0';
  strcpy (base, argv[1]);
  strcat (base, suffix);
  // ecriture de l'eps pour le coutours d=10 en mode remplissage
  ecrire_bezier_eps_2d (contours_bezier_d3, base, l, h);

  // simplification du contour d=30
  printf ("Simplification du contour (d=30.0) et affichage du résumé...\n");
  afficher_infos_bezier_2d (contours_bezier_d4);
  // on prépare le nom de fichier
  suffix[8] = '3';
  suffix[9] = '0';
  strcpy (base, argv[1]);
  strcat (base, suffix);
  // ecriture de l'eps pour le coutours d=10 en mode remplissage
  ecrire_bezier_eps_2d (contours_bezier_d4, base, l, h);


  // on va augmenter la surface de test en incluant les fichiers eps
  // dans la sortie du programme de test
  suffix[8] = '0';
  suffix[9] = '_';
  FILE *sortie = fopen (base, "r");
  if (sortie == NULL) {
    perror ("fopen:");
    exit (1);
  }
  char c;
  c = fgetc (sortie);
  while (c != EOF) {
    printf ("%c", c);
    c = fgetc (sortie);
  }
  fclose (sortie);

  suffix[8] = '1';
  suffix[9] = '_';
  sortie = fopen (base, "r");
  if (sortie == NULL) {
    perror ("fopen:");
    exit (1);
  }
  c = fgetc (sortie);
  while (c != EOF) {
    printf ("%c", c);
    c = fgetc (sortie);
  }
  fclose (sortie);

  suffix[8] = '3';
  suffix[9] = '_';
  sortie = fopen (base, "r");
  if (sortie == NULL) {
    perror ("fopen:");
    exit (1);
  }
  c = fgetc (sortie);
  while (c != EOF) {
    printf ("%c", c);
    c = fgetc (sortie);
  }
  fclose (sortie);

  suffix[8] = '1';
  suffix[9] = '0';
  sortie = fopen (base, "r");
  if (sortie == NULL) {
    perror ("fopen:");
    exit (1);
  }
  c = fgetc (sortie);
  while (c != EOF) {
    printf ("%c", c);
    c = fgetc (sortie);
  }
  fclose (sortie);

  suffix[8] = '3';
  suffix[9] = '0';
  sortie = fopen (base, "r");
  if (sortie == NULL) {
    perror ("fopen:");
    exit (1);
  }
  c = fgetc (sortie);
  while (c != EOF) {
    printf ("%c", c);
    c = fgetc (sortie);
  }
  fclose (sortie);

  // inutile car le processus va se fermer
  // free_multi_contours (contours_structure);
  // free_multi_contours (contours_simplifie_1);
  // free_multi_contours (contours_simplifie_2);

  free (test_image.tab);
  return 0;
}
