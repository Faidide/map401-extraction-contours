/*////////////////////////////////////////////////////////////////////////////

         /$$      /$$  /$$$$$$  /$$$$$$$  /$$   /$$  /$$$$$$    /$$
        | $$$    /$$$ /$$__  $$| $$__  $$| $$  | $$ /$$$_  $$ /$$$$
        | $$$$  /$$$$| $$  \ $$| $$  \ $$| $$  | $$| $$$$\ $$|_  $$
        | $$ $$/$$ $$| $$$$$$$$| $$$$$$$/| $$$$$$$$| $$ $$ $$  | $$
        | $$  $$$| $$| $$__  $$| $$____/ |_____  $$| $$\ $$$$  | $$
        | $$\  $ | $$| $$  | $$| $$            | $$| $$ \ $$$  | $$
        | $$ \/  | $$| $$  | $$| $$            | $$|  $$$$$$/ /$$$$$$
        |__/     |__/|__/  |__/|__/            |__/ \______/ |______/

/////////////////////////////////////////////////////////////////////////// */
// Fichier: test_calcul_contour.c
// Description: Implémentation des tests témoins du paquetage de calcul de
// contours
///////////////////////////////////////////////////////////////////////////

#include "calcul_contour.h"
#include "image.h"
#include <stdio.h>
#include <string.h>

int main (int argc, char *argv[]) {
  if (argc != 2) {
    printf ("usage: %s [image.pbm]\n", argv[0]);
    return 0;
  }

  printf ("Lancement du prog de test!\n");

  // lecture de l'image à tester
  Image test_image = lire_fichier_image (argv[1]);

  // affichage de l'image à tester
  printf ("Affichage initial de l'image.\n");
  ecrire_image (test_image);

  // lancement du calcul du contours
  Image img_contours = dessiner_contours (test_image);

  ecrire_image (test_image);

  // affichage du contours
  printf ("Affichage du contours.\n");
  ecrire_image (img_contours);

  // lancement du calcul de contours qui va le sauvarger en memoire
  printf ("Calcul du contours avec sauvegarde en memoire.\n");
  Contours *contours_structure = calculer_contours (test_image);

  // ecriture du fichier de contours
  printf ("Ecriture de la structure de donnee dans un fichier contours:\n");
  char suffix[10] = ".contours";
  char base[256];
  strcpy (base, argv[1]);
  strcat (base, suffix);

  printf ("%s\n", base);

  ecrire_contour (contours_structure, base);

  free_contours (contours_structure);

  free (img_contours.tab);
  free (test_image.tab);

  FILE *sortie = fopen (base, "r");

  if (sortie == NULL) {
    perror ("fopen:");
    exit (1);
  }

  char c;

  c = fgetc (sortie);
  while (c != EOF) {
    printf ("%c", c);
    c = fgetc (sortie);
  }

  fclose (sortie);

  return 0;
}
