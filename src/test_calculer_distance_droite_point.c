/*////////////////////////////////////////////////////////////////////////////

         /$$      /$$  /$$$$$$  /$$$$$$$  /$$   /$$  /$$$$$$    /$$
        | $$$    /$$$ /$$__  $$| $$__  $$| $$  | $$ /$$$_  $$ /$$$$
        | $$$$  /$$$$| $$  \ $$| $$  \ $$| $$  | $$| $$$$\ $$|_  $$
        | $$ $$/$$ $$| $$$$$$$$| $$$$$$$/| $$$$$$$$| $$ $$ $$  | $$
        | $$  $$$| $$| $$__  $$| $$____/ |_____  $$| $$\ $$$$  | $$
        | $$\  $ | $$| $$  | $$| $$            | $$| $$ \ $$$  | $$
        | $$ \/  | $$| $$  | $$| $$            | $$|  $$$$$$/ /$$$$$$
        |__/     |__/|__/  |__/|__/            |__/ \______/ |______/

/////////////////////////////////////////////////////////////////////////// */
// Fichier: test_calculer_distance_droite_point.c
// Description: Implémentation du test du calcul de distance droite point
///////////////////////////////////////////////////////////////////////////

#include "geom2d.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main (int argc, char *argv[]) {
  if (argc != 7) {
    printf ("usage (floats only): %s [A.x] [A.y] [B.x] [B.y] [P.x] [P.y]\n",
            argv[0]);
    return 0;
  }

  Point A = {strtof (argv[1], NULL), strtof (argv[2], NULL)};
  Point B = {strtof (argv[3], NULL), strtof (argv[4], NULL)};
  Point C = {strtof (argv[5], NULL), strtof (argv[6], NULL)};

  double distance = distance_point_segment (A, B, C);

  printf ("La distance est de: %f\n", distance);

  return 0;
}
