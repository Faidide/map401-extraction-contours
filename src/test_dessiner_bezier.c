/*////////////////////////////////////////////////////////////////////////////

         /$$      /$$  /$$$$$$  /$$$$$$$  /$$   /$$  /$$$$$$    /$$
        | $$$    /$$$ /$$__  $$| $$__  $$| $$  | $$ /$$$_  $$ /$$$$
        | $$$$  /$$$$| $$  \ $$| $$  \ $$| $$  | $$| $$$$\ $$|_  $$
        | $$ $$/$$ $$| $$$$$$$$| $$$$$$$/| $$$$$$$$| $$ $$ $$  | $$
        | $$  $$$| $$| $$__  $$| $$____/ |_____  $$| $$\ $$$$  | $$
        | $$\  $ | $$| $$  | $$| $$            | $$| $$ \ $$$  | $$
        | $$ \/  | $$| $$  | $$| $$            | $$|  $$$$$$/ /$$$$$$
        |__/     |__/|__/  |__/|__/            |__/ \______/ |______/

/////////////////////////////////////////////////////////////////////////// */
// Fichier: test_dessiner_bezier.c
// Description: Implémentation des tests pour generer une image a partir de bezier
///////////////////////////////////////////////////////////////////////////

#include "bezier.h"
#include <stdio.h>

int main (int argc, char *argv[]) {
  if (argc != 7 && argc !=9) {
    printf ("usage (floats only): %s c0.x c0.y c1.x c1.y c2.x c2.y\n", argv[0]);
    return 0;
  } else if (argc==7) {
    // argc == 7, degré 2
    // alloue une courbe de bezier 2d
    Bezier2D courbe_bezier_2d;

    // assigne les arguments au coord des pts de controle
    courbe_bezier_2d.c0.x = strtof (argv[1], NULL);
    courbe_bezier_2d.c0.y = strtof (argv[2], NULL);
    courbe_bezier_2d.c1.x = strtof (argv[3], NULL);
    courbe_bezier_2d.c1.y = strtof (argv[4], NULL);
    courbe_bezier_2d.c2.x = strtof (argv[5], NULL);
    courbe_bezier_2d.c2.y = strtof (argv[6], NULL);
    
    // genere l'image du contours 2D
    Image test_image = generer_courbe_bezier_2D (&courbe_bezier_2d, 0.001f, 100, 100);

    printf ("Affichage final de l'image.\n");
    ecrire_image (test_image);

    free (test_image.tab);

    return 0;
  } else {
    // argc == 9, degré 3
    // alloue une courbe de bezier 2d
    Bezier3D courbe_bezier_3d;

    // assigne les arguments au coord des pts de controle
    courbe_bezier_3d.c0.x = strtof (argv[1], NULL);
    courbe_bezier_3d.c0.y = strtof (argv[2], NULL);
    courbe_bezier_3d.c1.x = strtof (argv[3], NULL);
    courbe_bezier_3d.c1.y = strtof (argv[4], NULL);
    courbe_bezier_3d.c2.x = strtof (argv[5], NULL);
    courbe_bezier_3d.c2.y = strtof (argv[6], NULL);
    courbe_bezier_3d.c3.x = strtof (argv[7], NULL);
    courbe_bezier_3d.c3.y = strtof (argv[8], NULL);
    
    // genere l'image du contours 2D
    Image test_image = generer_courbe_bezier_3D (&courbe_bezier_3d, 0.001f, 100, 100);

    printf ("Affichage final de l'image.\n");
    ecrire_image (test_image);

    free (test_image.tab);

    return 0;
  }


}
