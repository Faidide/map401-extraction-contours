/*////////////////////////////////////////////////////////////////////////////

         /$$      /$$  /$$$$$$  /$$$$$$$  /$$   /$$  /$$$$$$    /$$
        | $$$    /$$$ /$$__  $$| $$__  $$| $$  | $$ /$$$_  $$ /$$$$
        | $$$$  /$$$$| $$  \ $$| $$  \ $$| $$  | $$| $$$$\ $$|_  $$
        | $$ $$/$$ $$| $$$$$$$$| $$$$$$$/| $$$$$$$$| $$ $$ $$  | $$
        | $$  $$$| $$| $$__  $$| $$____/ |_____  $$| $$\ $$$$  | $$
        | $$\  $ | $$| $$  | $$| $$            | $$| $$ \ $$$  | $$
        | $$ \/  | $$| $$  | $$| $$            | $$|  $$$$$$/ /$$$$$$
        |__/     |__/|__/  |__/|__/            |__/ \______/ |______/

/////////////////////////////////////////////////////////////////////////// */
// Fichier: test_dessiner_bezier.c
// Description: Implémentation des tests pour generer une image a partir de bezier
///////////////////////////////////////////////////////////////////////////

#include "bezier.h"
#include <stdio.h>

int main (int argc, char *argv[]) {
  if (argc != 3) {
    printf ("usage: %s [image.pbm] [distance(float)]\n", argv[0]);
    return 0;
  }

  double distance = strtof (argv[2], NULL);

  // lecture de l'image à tester
  Image test_image = lire_fichier_image (argv[1]);

  // affichage de l'image à tester
  printf ("Affichage initial de l'image.\n");
  ecrire_image (test_image);

  // lancement du calcul de contours qui va le sauvarger en memoire
  printf ("Calcul du multi contours avec sauvegarde en memoire.\n");

  MultiContours *contours_structure = calculer_multi_contours (test_image);
  afficher_infos_contours (contours_structure);

  // on récupère les dimensions de l'image
  UINT h = hauteur_image (test_image);
  UINT l = largeur_image (test_image);

  // simplifier le contours en bezier (pour d=1 et d=2)
  MultiContoursBezier2D * contours_bezier = simplification_bezier_multi_2D (contours_structure, distance);

  Image resultat = generer_image_multi_bezier_2d (contours_bezier, 0.01, l, h);

  printf ("Affichage initial de l'image.\n");
  ecrire_image (resultat);

  free (test_image.tab);

  return 0;

}
