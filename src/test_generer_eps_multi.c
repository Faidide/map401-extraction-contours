/*////////////////////////////////////////////////////////////////////////////

         /$$      /$$  /$$$$$$  /$$$$$$$  /$$   /$$  /$$$$$$    /$$
        | $$$    /$$$ /$$__  $$| $$__  $$| $$  | $$ /$$$_  $$ /$$$$
        | $$$$  /$$$$| $$  \ $$| $$  \ $$| $$  | $$| $$$$\ $$|_  $$
        | $$ $$/$$ $$| $$$$$$$$| $$$$$$$/| $$$$$$$$| $$ $$ $$  | $$
        | $$  $$$| $$| $$__  $$| $$____/ |_____  $$| $$\ $$$$  | $$
        | $$\  $ | $$| $$  | $$| $$            | $$| $$ \ $$$  | $$
        | $$ \/  | $$| $$  | $$| $$            | $$|  $$$$$$/ /$$$$$$
        |__/     |__/|__/  |__/|__/            |__/ \______/ |______/

/////////////////////////////////////////////////////////////////////////// */
// Fichier: test_generer_eps_multi.c
// Description: Implémentation des tests témoins du paquetage
///////////////////////////////////////////////////////////////////////////

#include "calcul_contour.h"
#include "image.h"
#include <stdio.h>
#include <string.h>

int main (int argc, char *argv[]) {
  if (argc != 2) {
    printf ("usage: %s [image.pbm]\n", argv[0]);
    return 0;
  }

  printf ("Lancement du prog de test!\n");

  // lecture de l'image à tester
  Image test_image = lire_fichier_image (argv[1]);

  // lancement du calcul de contours qui va le sauvarger en memoire
  printf ("Calcul du multi contours avec sauvegarde en memoire.\n");
  MultiContours *contours_structure = calculer_multi_contours (test_image);
  afficher_infos_contours (contours_structure);

  // ecriture du fichier de contours
  printf ("Ecriture de la structure de donnee dans un fichier contours:\n");
  char suffix[20] = "_1_multi.eps";
  char base[256];
  strcpy (base, argv[1]);
  strcat (base, suffix);

  UINT h = hauteur_image (test_image);
  UINT l = largeur_image (test_image);

  suffix[1] = '3';
  strcpy (base, argv[1]);
  strcat (base, suffix);

  ecrire_multi_contour_eps_mode_3 (contours_structure, base, l, h);


  // Inutile de free juste avant de fermer le programme
  // l'os va le faire tout seul
  // free_multi_contours (contours_structure);

  // free (test_image.tab);

  FILE *sortie = fopen (base, "r");

  if (sortie == NULL) {
    perror ("fopen:");
    exit (1);
  }

  char c;

  c = fgetc (sortie);
  while (c != EOF) {
    printf ("%c", c);
    c = fgetc (sortie);
  }

  fclose (sortie);

  return 0;
}
