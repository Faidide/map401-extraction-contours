/*////////////////////////////////////////////////////////////////////////////

         /$$      /$$  /$$$$$$  /$$$$$$$  /$$   /$$  /$$$$$$    /$$
        | $$$    /$$$ /$$__  $$| $$__  $$| $$  | $$ /$$$_  $$ /$$$$
        | $$$$  /$$$$| $$  \ $$| $$  \ $$| $$  | $$| $$$$\ $$|_  $$
        | $$ $$/$$ $$| $$$$$$$$| $$$$$$$/| $$$$$$$$| $$ $$ $$  | $$
        | $$  $$$| $$| $$__  $$| $$____/ |_____  $$| $$\ $$$$  | $$
        | $$\  $ | $$| $$  | $$| $$            | $$| $$ \ $$$  | $$
        | $$ \/  | $$| $$  | $$| $$            | $$|  $$$$$$/ /$$$$$$
        |__/     |__/|__/  |__/|__/            |__/ \______/ |______/

/////////////////////////////////////////////////////////////////////////// */
// Fichier: test_geom2d.c
// Description: Implémentation des tests témoins du paquetage de géométrie 2d
///////////////////////////////////////////////////////////////////////////

#include "geom2d.h"
#include <stdio.h>

int main (int argc, char *argv[]) {
  if (argc != 1) {
    printf ("usage: %s\n", argv[0]);
    return 0;
  }
  printf ("Lancement du prog de test!.\n");

  Vecteur V1 = {5.0, 5.0};
  Vecteur V2 = {7.0, 3.0};
  Point P1 = {10.0, 10.0};
  Point P2 = {8.0, -10.0};

  printf ("V1: %lf, %lf;\n", V1.x, V1.y);
  printf ("V2: %lf, %lf;\n", V2.x, V2.y);
  printf ("P1: %lf, %lf;\n", P1.x, P1.y);
  printf ("P2: %lf, %lf;\n", P2.x, P2.y);

  Point PT1 = set_point (-2.0, 7.0);
  printf ("PT1: %lf, %lf;\n", PT1.x, PT1.y);
  printf ("expected: -2.0, 7.0;\n");

  Point PT2 = add_point (P1, P2);
  printf ("PT2: %lf, %lf;\n", PT2.x, PT2.y);
  printf ("expected: 18.0, 0.0;\n");

  Vecteur VT3 = vect_bipoint (P1, P2);
  printf ("VT3: %lf, %lf;\n", VT3.x, VT3.y);
  printf ("expected: -2.0, -20.0;\n");

  Vecteur VT4 = add_vect (V1, V2);
  printf ("VT4: %lf, %lf;\n", VT4.x, VT4.y);
  printf ("expected: 12.0, 8.0;\n");

  Vecteur VT5 = multiplication_scalaire_vect (V1, 2.5);
  printf ("VT5: %lf, %lf;\n", VT5.x, VT5.y);
  printf ("expected: 12.5, 12.5;\n");

  double norme = norme_vecteur (V2);
  printf ("norme V2: %lf\n", norme);
  printf ("expected(approx): 7,615;\n");

  double distance_exp = distance (P1, P2);
  printf ("distanceP1/P2: %lf\n", distance_exp);
  printf ("expected(approx): 20,099;\n");

  return 0;
}
