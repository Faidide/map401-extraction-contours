/*////////////////////////////////////////////////////////////////////////////

         /$$      /$$  /$$$$$$  /$$$$$$$  /$$   /$$  /$$$$$$    /$$
        | $$$    /$$$ /$$__  $$| $$__  $$| $$  | $$ /$$$_  $$ /$$$$
        | $$$$  /$$$$| $$  \ $$| $$  \ $$| $$  | $$| $$$$\ $$|_  $$
        | $$ $$/$$ $$| $$$$$$$$| $$$$$$$/| $$$$$$$$| $$ $$ $$  | $$
        | $$  $$$| $$| $$__  $$| $$____/ |_____  $$| $$\ $$$$  | $$
        | $$\  $ | $$| $$  | $$| $$            | $$| $$ \ $$$  | $$
        | $$ \/  | $$| $$  | $$| $$            | $$|  $$$$$$/ /$$$$$$
        |__/     |__/|__/  |__/|__/            |__/ \______/ |______/

/////////////////////////////////////////////////////////////////////////// */
// Fichier: image.c
// Description: Implémentation des tests du paquetage image
///////////////////////////////////////////////////////////////////////////

#include "image.h"
#include <stdio.h>

int main (int argc, char *argv[]) {
  if (argc != 2) {
    printf ("usage: %s [image.bmp]\n", argv[0]);
    return 0;
  }

  Image test_image = lire_fichier_image (argv[1]);

  printf ("Affichage initial de l'image.\n");
  ecrire_image (test_image);

  printf ("Modification des pixels.\n");
  set_pixel_image (test_image, 1, 1, (Pixel)0);
  set_pixel_image (test_image, 1, 2, (Pixel)1);
  set_pixel_image (test_image, 1, 3, (Pixel)0);
  set_pixel_image (test_image, 1, 4, (Pixel)1);

  printf ("Affichage final de l'image.\n");
  ecrire_image (test_image);

  free (test_image.tab);

  return 0;
}
