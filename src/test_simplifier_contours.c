/*////////////////////////////////////////////////////////////////////////////

         /$$      /$$  /$$$$$$  /$$$$$$$  /$$   /$$  /$$$$$$    /$$
        | $$$    /$$$ /$$__  $$| $$__  $$| $$  | $$ /$$$_  $$ /$$$$
        | $$$$  /$$$$| $$  \ $$| $$  \ $$| $$  | $$| $$$$\ $$|_  $$
        | $$ $$/$$ $$| $$$$$$$$| $$$$$$$/| $$$$$$$$| $$ $$ $$  | $$
        | $$  $$$| $$| $$__  $$| $$____/ |_____  $$| $$\ $$$$  | $$
        | $$\  $ | $$| $$  | $$| $$            | $$| $$ \ $$$  | $$
        | $$ \/  | $$| $$  | $$| $$            | $$|  $$$$$$/ /$$$$$$
        |__/     |__/|__/  |__/|__/            |__/ \______/ |______/

/////////////////////////////////////////////////////////////////////////// */
// Fichier: test_simplifier_contours.c
// Description: Implémentation des tests témoins
///////////////////////////////////////////////////////////////////////////

#include "image.h"
#include "simplification.h"
#include <stdio.h>
#include <string.h>

int main (int argc, char *argv[]) {
  if (argc != 2) {
    printf ("usage: %s [image.pbm]\n", argv[0]);
    return 0;
  }

  printf ("Lancement du prog de test!\n");

  // lecture de l'image à tester
  Image test_image = lire_fichier_image (argv[1]);

  // affichage de l'image à tester
  printf ("Affichage initial de l'image.\n");
  ecrire_image (test_image);

  // lancement du calcul de contours qui va le sauvarger en memoire
  printf ("Calcul du multi contours avec sauvegarde en memoire.\n");
  MultiContours *contours_structure = calculer_multi_contours (test_image);
  afficher_infos_contours (contours_structure);

  // simplification du contour d=1
  printf ("Simplification du contour (d=1.0) et affichage du résumé...\n");
  MultiContours *contours_simplifie_1 =
      simplification_multi_contours (contours_structure, 1.0);
  afficher_infos_contours (contours_simplifie_1);

  // simplification du contour d=2
  printf ("Simplification du contour (d=2.0) et affichage du résumé...\n");
  MultiContours *contours_simplifie_2 =
      simplification_multi_contours (contours_structure, 2.0);
  afficher_infos_contours (contours_simplifie_2);

  // generation des fichiers eps
  char suffix[20] = "simp_1.eps";
  char base[256];

  // on récupère les dimensions de l'image
  UINT h = hauteur_image (test_image);
  UINT l = largeur_image (test_image);

  // on prépare le nom de fichier
  suffix[5] = '1';
  strcpy (base, argv[1]);
  strcat (base, suffix);

  // ecriture de l'eps pour le coutours d=1 en mode remplissage
  ecrire_multi_contour_eps_mode_3 (contours_simplifie_1, base, l, h);

  // on prépare le nom de fichier
  suffix[5] = '2';
  strcpy (base, argv[1]);
  strcat (base, suffix);

  // ecriture de l'eps pour le coutours d=2 en mode remplissage
  ecrire_multi_contour_eps_mode_3 (contours_simplifie_2, base, l, h);

  // liberation de la memoire et fin de fonction
  // free_multi_contours (contours_structure);
  // free_multi_contours (contours_simplifie_1);
  // free_multi_contours (contours_simplifie_2);
  free (test_image.tab);
  return 0;
}
