#!/bin/bash

echo -e "\033[33m========== \033[33m Execution des tests pour le paquetage \033[35mimage \033[33m ===========\033[0m"
printf "%-30.30s" "Execution des tests..."
./../build/bin/test_image ../res/caractere2.pbm > test_image.output
echo -e "\033[32mReussite\033[0m"
printf "%-30.30s" "Verification des sorties..."
if diff test_image.output test_image.expected
then
	echo -e "\033[32mReussite\033[0m"
else
	echo -e "\033[31mEchec\033[0m"
	exit 1
fi



echo -e "\033[33m========== \033[33m Execution des tests pour le paquetage \033[35mgeom2d \033[33m ==========\033[0m"
printf "%-30.30s" "Execution des tests de base..."
./../build/bin/test_geom2d > test_geom2d.output
echo -e "\033[32mReussite\033[0m"
printf "%-30.30s" "Verification des sorties..."
if diff test_geom2d.output test_geom2d.expected
then
	echo -e "\033[32mReussite\033[0m"
else
	echo -e "\033[31mEchec\033[0m"
	exit 1
fi

# test avec point sur la ligne
printf "%-30.30s" "Execution du test distance point-segment 1..."
../build/bin/test_calculer_distance_droite_point 0.0 2.0 1.2 3.2 1.5 2.0 > test_geom2d_distance.output
echo -e "\033[32mReussite\033[0m"
printf "%-30.30s" "Verification des sorties..."
if diff test_geom2d_distance.output test_geom2d_distance.expected
then
	echo -e "\033[32mReussite\033[0m"
else
	echo -e "\033[31mEchec\033[0m"
	exit 1
fi

# test avec point en dessous de la ligne
printf "%-30.30s" "Execution du test distance point-segment 2..."
../build/bin/test_calculer_distance_droite_point 0.0 2.0 1.2 3.2 0.5 0.5 > test_geom2d_distance2.output
echo -e "\033[32mReussite\033[0m"
printf "%-30.30s" "Verification des sorties..."
if diff test_geom2d_distance2.output test_geom2d_distance2.expected
then
	echo -e "\033[32mReussite\033[0m"
else
	echo -e "\033[31mEchec\033[0m"
	exit 1
fi

# test avec point en dessus de la ligne
printf "%-30.30s" "Execution du test distance point-segment 3..."
../build/bin/test_calculer_distance_droite_point 0.0 2.0 1.2 3.2 3.0 4.0 > test_geom2d_distance3.output
echo -e "\033[32mReussite\033[0m"
printf "%-30.30s" "Verification des sorties..."
if diff test_geom2d_distance3.output test_geom2d_distance3.expected
then
	echo -e "\033[32mReussite\033[0m"
else
	echo -e "\033[31mEchec\033[0m"
	exit 1
fi



echo -e "\033[33m====== \033[33m Execution des tests pour le paquetage \033[35mcalcul_contour \033[33m ======\033[0m"

printf "Verification des sorties...\n"

printf "%-30.30s" "Image simpleforme..."
./../build/bin/test_calcul_contour ../res/simpleforme.pbm > test_calcul_contour_simpleforme.output
if diff test_calcul_contour_simpleforme.output test_calcul_contour_simpleforme.expected
then
	echo -e "\033[32mReussite\033[0m"
else
	echo -e "\033[31mEchec\033[0m"
	exit 1
fi

printf "%-30.30s" "Image a..."
./../build/bin/test_calcul_contour ../res/ascii_a.pbm > test_calcul_contour_ascii_a.output
if diff test_calcul_contour_ascii_a.output test_calcul_contour_ascii_a.expected
then
	echo -e "\033[32mReussite\033[0m"
else
	echo -e "\033[31mEchec\033[0m"
	exit 1
fi

printf "%-30.30s" "Image b..."
./../build/bin/test_calcul_contour ../res/ascii_b.pbm > test_calcul_contour_ascii_b.output
if diff test_calcul_contour_ascii_b.output test_calcul_contour_ascii_b.expected
then
	echo -e "\033[32mReussite\033[0m"
else
	echo -e "\033[31mEchec\033[0m"
	exit 1
fi

printf "%-30.30s" "Image p..."
./../build/bin/test_calcul_contour ../res/ascii_p.pbm > test_calcul_contour_p.output
if diff test_calcul_contour_p.output test_calcul_contour_p.expected
then
	echo -e "\033[32mReussite\033[0m"
else
	echo -e "\033[31mEchec\033[0m"
	exit 1
fi

printf "%-30.30s" "Image v..."
./../build/bin/test_calcul_contour ../res/ascii_v.pbm > test_calcul_contour_v.output
if diff test_calcul_contour_v.output test_calcul_contour_v.expected
then
	echo -e "\033[32mReussite\033[0m"
else
	echo -e "\033[31mEchec\033[0m"
	exit 1
fi


printf "%-30.30s" "Image montagne..."
./../build/bin/test_calcul_contour ../res/mountain.pbm > test_calcul_contour_mountain.output
if diff test_calcul_contour_mountain.output test_calcul_contour_mountain.expected
then
	echo -e "\033[32mReussite\033[0m"
else
	echo -e "\033[31mEchec\033[0m"
	exit 1
fi

printf "%-30.30s" "Image chocolat..."
./../build/bin/test_calcul_contour ../res/chocolate.pbm > test_calcul_contour_chocolate.output
if diff test_calcul_contour_chocolate.output test_calcul_contour_chocolate.expected
then
	echo -e "\033[32mReussite\033[0m"
else
	echo -e "\033[31mEchec\033[0m"
	exit 1
fi


printf "%-30.30s" "Image x..."
./../build/bin/test_calcul_contour ../res/ascii_x.pbm > test_calcul_contour_x.output
if diff test_calcul_contour_x.output test_calcul_contour_x.expected
then
	echo -e "\033[32mReussite\033[0m"
else
	echo -e "\033[31mEchec\033[0m"
	exit 1
fi


echo -e "\033[33m====== \033[33m Execution des tests pour le paquetage \033[35mcalcul_contour_multi \033[33m ======\033[0m"

printf "Execution des tests ...\n"

printf "%-30.30s" "Image image2_poly..."
./../build/bin/test_calcul_multi_contour ../res/image2_poly.pbm > test_calcul_multi_contours_image2_poly.output
if diff test_calcul_multi_contours_image2_poly.output test_calcul_multi_contours_image2_poly.expected
then
	echo -e "\033[32mReussite\033[0m"
else
	echo -e "\033[31mEchec\033[0m"
	exit 1
fi

printf "%-30.30s" "Image image1_poly..."
./../build/bin/test_calcul_multi_contour ../res/image1_poly.pbm > test_calcul_multi_contours_image1_poly.output
if diff test_calcul_multi_contours_image1_poly.output test_calcul_multi_contours_image1_poly.expected
then
	echo -e "\033[32mReussite\033[0m"
else
	echo -e "\033[31mEchec\033[0m"
	exit 1
fi

printf "%-30.30s" "Image Bugs_Bunny..."
./../build/bin/test_calcul_multi_contour ../res/Bugs_Bunny.pbm > test_calcul_multi_contours_Bugs_Bunny.output
if diff test_calcul_multi_contours_Bugs_Bunny.output test_calcul_multi_contours_Bugs_Bunny.expected
then
	echo -e "\033[32mReussite\033[0m"
else
	echo -e "\033[31mEchec\033[0m"
	exit 1
fi


printf "%-30.30s" "Image Charlot..."
./../build/bin/test_calcul_multi_contour ../res/Charlot.pbm > test_calcul_multi_contours_Charlot.output
if diff test_calcul_multi_contours_Charlot.output test_calcul_multi_contours_Charlot.expected
then
	echo -e "\033[32mReussite\033[0m"
else
	echo -e "\033[31mEchec\033[0m"
	exit 1
fi

printf "%-30.30s" "Image Pink_Panther..."
./../build/bin/test_calcul_multi_contour ../res/Pink_Panther.pbm > test_calcul_multi_contours_Pink_Panther.output
if diff test_calcul_multi_contours_Pink_Panther.output test_calcul_multi_contours_Pink_Panther.expected
then
	echo -e "\033[32mReussite\033[0m"
else
	echo -e "\033[31mEchec\033[0m"
	exit 1
fi


printf "%-30.30s" "Image animaux..."
./../build/bin/test_calcul_multi_contour ../res/animaux.pbm > test_calcul_multi_contours_animaux.output
if diff test_calcul_multi_contours_animaux.output test_calcul_multi_contours_animaux.expected
then
	echo -e "\033[32mReussite\033[0m"
else
	echo -e "\033[31mEchec\033[0m"
	exit 1
fi


printf "%-30.30s" "Image damier_4_5_1..."
./../build/bin/test_calcul_multi_contour ../res/damier_4_5_1.pbm > test_calcul_multi_contours_damier_4_5_1.output
if diff test_calcul_multi_contours_damier_4_5_1.output test_calcul_multi_contours_damier_4_5_1.expected
then
	echo -e "\033[32mReussite\033[0m"
else
	echo -e "\033[31mEchec\033[0m"
	exit 1
fi


printf "%-30.30s" "Image deux-des..."
./../build/bin/test_calcul_multi_contour ../res/deux-des.pbm > test_calcul_multi_contours_deux-des.output
if diff test_calcul_multi_contours_deux-des.output test_calcul_multi_contours_deux-des.expected
then
	echo -e "\033[32mReussite\033[0m"
else
	echo -e "\033[31mEchec\033[0m"
	exit 1
fi


printf "%-30.30s" "Image dessin-delius..."
./../build/bin/test_calcul_multi_contour ../res/dessin-delius.pbm > test_calcul_multi_contours_dessin-delius.output
if diff test_calcul_multi_contours_dessin-delius.output test_calcul_multi_contours_dessin-delius.expected
then
	echo -e "\033[32mReussite\033[0m"
else
	echo -e "\033[31mEchec\033[0m"
	exit 1
fi


printf "%-30.30s" "Image gai-luron..."
./../build/bin/test_calcul_multi_contour ../res/gai-luron.pbm > test_calcul_multi_contours_gai-luron.output
if diff test_calcul_multi_contours_gai-luron.output test_calcul_multi_contours_gai-luron.expected
then
	echo -e "\033[32mReussite\033[0m"
else
	echo -e "\033[31mEchec\033[0m"
	exit 1
fi

printf "%-30.30s" "Image papillon2..."
./../build/bin/test_calcul_multi_contour ../res/papillon2.pbm > test_calcul_multi_contours_papillon2.output
if diff test_calcul_multi_contours_papillon2.output test_calcul_multi_contours_papillon2.expected
then
	echo -e "\033[32mReussite\033[0m"
else
	echo -e "\033[31mEchec\033[0m"
	exit 1
fi

printf "%-30.30s" "Image montagne..."
./../build/bin/test_calcul_multi_contour ../res/mountain.pbm > test_calcul_multi_contours_mountain.output
if diff test_calcul_multi_contours_mountain.output test_calcul_multi_contours_mountain.expected
then
	echo -e "\033[32mReussite\033[0m"
else
	echo -e "\033[31mEchec\033[0m"
	exit 1
fi

printf "%-30.30s" "Image chocolat..."
./../build/bin/test_calcul_multi_contour ../res/chocolate.pbm > test_calcul_multi_contours_chocolat.output
if diff test_calcul_multi_contours_chocolat.output test_calcul_multi_contours_chocolat.expected
then
	echo -e "\033[32mReussite\033[0m"
else
	echo -e "\033[31mEchec\033[0m"
	exit 1
fi

echo -e "\033[33m====== \033[33m Execution des tests pour le paquetage \033[35mgenerer_eps_multicontour \033[33m ======\033[0m"

printf "Verification des sorties...\n"

printf "%-30.30s" "Image image1_poly..."
./../build/bin/test_generer_eps_multi ../res/image1_poly.pbm > test_generer_eps_multi_image1_poly.output
if diff test_generer_eps_multi_image1_poly.output test_generer_eps_multi_image1_poly.expected
then
	echo -e "\033[32mReussite\033[0m"
else
	echo -e "\033[31mEchec\033[0m"
	exit 1
fi

printf "%-30.30s" "Image image2_poly..."
./../build/bin/test_generer_eps_multi ../res/image2_poly.pbm > test_generer_eps_multi_image2_poly.output
if diff test_generer_eps_multi_image2_poly.output test_generer_eps_multi_image2_poly.expected
then
	echo -e "\033[32mReussite\033[0m"
else
	echo -e "\033[31mEchec\033[0m"
	exit 1
fi

printf "%-30.30s" "Image France_Regions..."
./../build/bin/test_generer_eps_multi ../res/FranceRegions.pbm > test_generer_eps_multi_FranceRegions.output
if diff test_generer_eps_multi_FranceRegions.output test_generer_eps_multi_FranceRegions.expected
then
	echo -e "\033[32mReussite\033[0m"
else
	echo -e "\033[31mEchec\033[0m"
	exit 1
fi

printf "%-30.30s" "Image Droopy_Wolf..."
./../build/bin/test_generer_eps_multi ../res/Droopy_Wolf.pbm > test_generer_eps_multi_Droopy_Wolf.output
if diff test_generer_eps_multi_Droopy_Wolf.output test_generer_eps_multi_Droopy_Wolf.expected
then
	echo -e "\033[32mReussite\033[0m"
else
	echo -e "\033[31mEchec\033[0m"
	exit 1
fi

echo -e "\033[33m====== \033[33m Execution des tests pour le paquetage \033[35msimplification_contours \033[33m ======\033[0m"

printf "Execution des tests ...\n"

printf "%-30.30s" "Image image2_poly..."
./../build/bin/test_simplifier_contours ../res/image2_poly.pbm > test_simplifier_contours_image2_poly.output
if diff test_calcul_multi_contours_image2_poly.output test_calcul_multi_contours_image2_poly.expected
then
	echo -e "\033[32mReussite\033[0m"
else
	echo -e "\033[31mEchec\033[0m"
	exit 1
fi

printf "%-30.30s" "Image Bugs_Bunny..."
./../build/bin/test_simplifier_contours ../res/Bugs_Bunny.pbm > test_simplifier_contours_Bugs_Bunny.output
if diff test_simplifier_contours_Bugs_Bunny.output test_simplifier_contours_Bugs_Bunny.expected
then
	echo -e "\033[32mReussite\033[0m"
else
	echo -e "\033[31mEchec\033[0m"
	exit 1
fi


printf "%-30.30s" "Image Charlot..."
./../build/bin/test_simplifier_contours ../res/Charlot.pbm > test_simplifier_contours_Charlot.output
if diff test_simplifier_contours_Charlot.output test_simplifier_contours_Charlot.expected
then
	echo -e "\033[32mReussite\033[0m"
else
	echo -e "\033[31mEchec\033[0m"
	exit 1
fi

printf "%-30.30s" "Image Pink_Panther..."
./../build/bin/test_simplifier_contours ../res/Pink_Panther.pbm > test_simplifier_contours_Pink_Panther.output
if diff test_simplifier_contours_Pink_Panther.output test_simplifier_contours_Pink_Panther.expected
then
	echo -e "\033[32mReussite\033[0m"
else
	echo -e "\033[31mEchec\033[0m"
	exit 1
fi

printf "%-30.30s" "Image image_poly_tache6..."
./../build/bin/test_simplifier_contours ../res/image_poly_tache6.pbm > test_simplifier_contours_image_poly_tache6.output
if diff test_simplifier_contours_image_poly_tache6.output test_simplifier_contours_image_poly_tache6.expected
then
	echo -e "\033[32mReussite\033[0m"
else
	echo -e "\033[31mEchec\033[0m"
	exit 1
fi

printf "%-30.30s" "Image elephant-gotlib..."
./../build/bin/test_simplifier_contours ../res/elephant-gotlib.pbm > test_simplifier_contours_elephant-gotlib.output
if diff test_simplifier_contours_elephant-gotlib.output test_simplifier_contours_elephant-gotlib.expected
then
	echo -e "\033[32mReussite\033[0m"
else
	echo -e "\033[31mEchec\033[0m"
	exit 1
fi

printf "%-30.30s" "Image goudyini-A..."
./../build/bin/test_simplifier_contours ../res/goudyini-A.pbm > test_simplifier_contours_goudyini-A.output
if diff test_simplifier_contours_goudyini-A.output test_simplifier_contours_goudyini-A.expected
then
	echo -e "\033[32mReussite\033[0m"
else
	echo -e "\033[31mEchec\033[0m"
	exit 1
fi

printf "%-30.30s" "Image JoaquimHock-LesArbres..."
./../build/bin/test_simplifier_contours ../res/JoaquimHock-LesArbres.pbm > test_simplifier_contours_JoaquimHock-LesArbres.output
if diff test_simplifier_contours_JoaquimHock-LesArbres.output test_simplifier_contours_JoaquimHock-LesArbres.expected
then
	echo -e "\033[32mReussite\033[0m"
else
	echo -e "\033[31mEchec\033[0m"
	exit 1
fi

printf "%-30.30s" "Image cheval..."
./../build/bin/test_simplifier_contours ../res/cheval.pbm > test_simplifier_contours_cheval.output
if diff test_simplifier_contours_cheval.output test_simplifier_contours_cheval.expected
then
	echo -e "\033[32mReussite\033[0m"
else
	echo -e "\033[31mEchec\033[0m"
	exit 1
fi

printf "%-30.30s" "Image papillon2..."
./../build/bin/test_simplifier_contours ../res/papillon2.pbm > test_simplifier_contours_papillon2.output
if diff test_simplifier_contours_papillon2.output test_simplifier_contours_papillon2.expected
then
	echo -e "\033[32mReussite\033[0m"
else
	echo -e "\033[31mEchec\033[0m"
	exit 1
fi

echo -e "\033[33m====== \033[33m Execution des tests pour le paquetage \033[35m dessiner_bezier \033[33m ======\033[0m"

printf "Execution des tests ...\n"

printf "%-30.30s" "Image avec une courbe de deg 2..."
./../build/bin/test_dessiner_bezier 0.0 0.0 0.0 100.0 100.0 100.0 > test_dessiner_bezier_2d.output
if diff test_dessiner_bezier_2d.output test_dessiner_bezier_2d.expected
then
	echo -e "\033[32mReussite\033[0m"
else
	echo -e "\033[31mEchec\033[0m"
	exit 1
fi

printf "%-30.30s" "Image avec une courbe de deg 3..."
./../build/bin/test_dessiner_bezier 0.0 0.0 0.0 100.0 100.0 0.0 100.0 100.0 > test_dessiner_bezier_3d.output
if diff test_dessiner_bezier_3d.output test_dessiner_bezier_3d.expected
then
	echo -e "\033[32mReussite\033[0m"
else
	echo -e "\033[31mEchec\033[0m"
	exit 1
fi

echo -e "\033[33m====== \033[33m Execution des tests pour le paquetage \033[35m dessiner_bezier_2 \033[33m ======\033[0m"

printf "Execution des tests ...\n"

printf "%-30.30s" "Image ascii_a..."
./../build/bin/test_dessiner_bezier_2 ../res/ascii_a.pbm 0.2 > test_dessiner_bezier_2_ascii_a.output
if diff test_dessiner_bezier_2_ascii_a.output test_dessiner_bezier_2_ascii_a.expected
then
	echo -e "\033[32mReussite\033[0m"
else
	echo -e "\033[31mEchec\033[0m"
	exit 1
fi

printf "%-30.30s" "Image cheval..."
./../build/bin/test_dessiner_bezier_2 ../res/cheval.pbm 0.2 > test_dessiner_bezier_2_cheval.output
if diff test_dessiner_bezier_2_cheval.output test_dessiner_bezier_2_cheval.expected
then
	echo -e "\033[32mReussite\033[0m"
else
	echo -e "\033[31mEchec\033[0m"
	exit 1
fi

echo -e "\033[33m====== \033[33m Execution des tests pour le paquetage \033[35m simplification bezier \033[33m ======\033[0m"

printf "Execution des tests ...\n"

printf "%-30.30s" "Image Asterix..."
./../build/bin/test_bezier_simplification ../res/Asterix3.pbm > test_simplif_bezier_Asterix3.output
if diff test_simplif_bezier_Asterix3.output test_simplif_bezier_Asterix3.expected
then
	echo -e "\033[32mReussite\033[0m"
else
	echo -e "\033[31mEchec\033[0m"
	exit 1
fi

printf "%-30.30s" "Image lettre-L-cursive..."
./../build/bin/test_bezier_simplification ../res/lettre-L-cursive.pbm > test_simplif_bezier_lettre-L-cursive.output
if diff test_simplif_bezier_lettre-L-cursive.output test_simplif_bezier_lettre-L-cursive.expected
then
	echo -e "\033[32mReussite\033[0m"
else
	echo -e "\033[31mEchec\033[0m"
	exit 1
fi

printf "%-30.30s" "Image Picasso-ColombesDeLaPaix..."
./../build/bin/test_bezier_simplification ../res/Picasso-ColombesDeLaPaix.pbm > test_simplif_bezier_Picasso-ColombesDeLaPaix.output
if diff test_simplif_bezier_Picasso-ColombesDeLaPaix.output test_simplif_bezier_Picasso-ColombesDeLaPaix.expected
then
	echo -e "\033[32mReussite\033[0m"
else
	echo -e "\033[31mEchec\033[0m"
	exit 1
fi

echo " "
echo -e "\033[32mTous les tests on été passés avec succès!\033[0m"
